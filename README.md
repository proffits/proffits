# ProfFitS - Fit peak Profiles to Scattering data #

ProfFitS is a python package for deconvolving the instrumental resolution function from neutron scattering data from time-of-flight instruments.

## How to use ##

ProfFitS is still in the early stages of development; its API and functionality are still subject to change.
It will not be released as a PyPI package with setup.py etc until it has stabilised.
If you want to use it before then, clone the `proffits` repository as a submodule of your data repository
(you do keep your scientific data in a repository, right ;) ) and write code that says
    import proffits
or
    from proffits import *
