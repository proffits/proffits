import numpy as np
from nose import SkipTest
from nose.tools import assert_almost_equal, assert_less

from proffits import Gudrun_extra

from proffits.fileio.test.get_testdata import testdata_file as datafile


def test_subtractSmallAngleScattering(fit_qmin=0.2, fit_qmax=1.1):
    dcs = Gudrun_extra.DCS_extra(datafile('Fe75Co25_GEM57666.dcs01'))
    A, power = dcs.subtractSmallAngleScattering((fit_qmin, fit_qmax))
    assert_almost_equal(A, 0.206, places=2)
    assert_almost_equal(power, -1.24, places=1)
    for bank in dcs.banks[2:]:
        iqmax = np.min(np.where(bank.q > fit_qmax))
        if iqmax == 0:
            break
        # sexpt should look like -q
        chi2 = np.sum(((bank.sexpt[:iqmax] + bank.q[iqmax]) /
                       bank.sexpt_errs[:iqmax])**2) / iqmax
        assert_less(chi2, 5000)


def test_subtract_incoherent_and_SANS():
    # The subtract_incoherent_and_SANS function does not work very well...
    raise SkipTest
