# -*- coding: utf-8 -*-

import numpy as np
from numpy.testing import assert_array_equal, assert_array_almost_equal
from nose import SkipTest
from nose.tools import (assert_equal, assert_is_instance, assert_greater_equal,
                        assert_less_equal)

from proffits.tofprofiles import misc
from proffits.fileio.inst import Inst2File
from proffits import fitDr
from proffits.fileio import Gudrun, merge_params

from proffits.fileio.test.get_testdata import testdata_file as datafile

r = np.linspace(0.01, 10, 1000)  # 0.2, 0.4, ..., 5.0
nr = len(r)


def assert_valid_FitConstraint(constraint):
    assert_is_instance(constraint, fitDr.FitConstraint)
    nx, = constraint.x.shape
    assert_equal(constraint.y.shape, (nx,))
    assert_equal(constraint.m.shape, (nx, nr))
    assert np.isscalar(constraint.w) or constraint.w.shape == (nx,)
    if hasattr(constraint, 'q'):
        assert constraint.q.shape == (nx,)


class bogusSQdata:
    def __init__(self, q, s, e=None):
        self.q = q
        self.sexpt = s
        if e is not None:
            self.sexpt_errs = e


class bogusSQdata_Gr:
    def __init__(self, q, r, gr, e=None):
        self.q = q
        sqr = np.sin(q[:, np.newaxis] * r) * misc.dx(r)
        self.sexpt = np.dot(sqr, gr)
        if e is not None:
            self.sexpt_errs = e


def test_rmin():
    rmin = fitDr.MinimumRConstraint(r, rmin=1.001)
    assert_valid_FitConstraint(rmin)
    assert_equal(len(rmin.y), 100)
    assert_array_equal(rmin.transform(np.ones_like(r)), np.ones_like(rmin.y))


def test_qmin():
    qmin = fitDr.MinimumQConstraint(r, dq=0.01, qmin=0.7001)
    assert_valid_FitConstraint(qmin)
    assert_equal(len(qmin.y), 70)
    gr_highfreq = np.sin(30 * np.pi * r)
    # Not a very good test but at least it catches the incorrect divide by r
    assert_array_almost_equal(qmin.transform(gr_highfreq),
                              np.zeros_like(qmin.y), decimal=1)


def test_smoothing():
    smooth = fitDr.SmoothnessConstraint(r)
    assert_valid_FitConstraint(smooth)
    # smoothness of a constant is 0
    assert_array_almost_equal(smooth.transform(np.ones_like(r)),
                              np.zeros((nr - 2,)))
    # smoothness of a fixed slope is 0
    assert_array_almost_equal(smooth.transform(r * 4),
                              np.zeros((nr - 2,)))


def test_asymptote():
    asy = fitDr.AsymptoteConstraint(r, rmin=5.001)
    assert_valid_FitConstraint(asy)
    assert_equal(len(asy.y), 500)
    assert_array_equal(asy.transform(np.ones_like(r)), np.ones_like(asy.y))


def test_SQconstraint():
    data = bogusSQdata(np.linspace(0.5, 10, 20), np.random.rand(20))
    params = Inst2File(datafile('GEM56239.inst'))
    sqc = fitDr.SQDataConstraint(r, data, params[3])
    assert_valid_FitConstraint(sqc)


def test_SQconstraint_qideal():
    qideal = np.linspace(1, 15, 30)
    data = bogusSQdata(np.linspace(3, 10, 20), np.random.rand(20))
    params = Inst2File(datafile('GEM56239.inst'))
    sqc = fitDr.SQDataConstraint(r, data, params[3], qideal)
    assert_valid_FitConstraint(sqc)
    assert_greater_equal(sqc.qideal[0], 2)
    assert_less_equal(sqc.qideal[-1], 11)


def test_Gudrun_params_with_linToF():
    groups = Gudrun.GudrunGroupFile(datafile('gudrun_grp.dat'))
    params = Inst2File(datafile('GEM56239.inst'))
    dcs = Gudrun.DCS(datafile('Fe75Co25_GEM57666.dcs01'))
    pairs = merge_params.merge_grp_with_inst(groups, params)

    # Try the beginning of the first valid bank
    mrg = pairs[0]
    dcsbank = dcs.banks[mrg.igroup]
    # Cut down to a manageable size
    dcsbank.q = dcsbank.q[:100]
    dcsbank.sexpt = dcsbank.sexpt[:100]
    dcsbank.sexpt_errs = dcsbank.sexpt_errs[:100]
    dcsbank.qsexpt = dcsbank.qsexpt[:100]
    dcsbank.qsexpt_errs = dcsbank.qsexpt_errs[:100]
    sqc = fitDr.SQDataConstraint(r, dcsbank, mrg)
    assert_valid_FitConstraint(sqc)

    # Try the end of the last valid bank
    mrg = pairs[4]
    dcsbank = dcs.banks[mrg.igroup]
    # Cut down to a manageable size
    dcsbank.q = dcsbank.q[-100:]
    dcsbank.sexpt = dcsbank.sexpt[-100:]
    dcsbank.sexpt_errs = dcsbank.sexpt_errs[-100:]
    dcsbank.qsexpt = dcsbank.qsexpt[-100:]
    dcsbank.qsexpt_errs = dcsbank.qsexpt_errs[-100:]
    sqc = fitDr.SQDataConstraint(r, dcsbank, mrg)
    assert_valid_FitConstraint(sqc)


def test_simple_fit(nr=100):
    r = np.linspace(0.1, 10, nr)
    rmin_c = fitDr.MinimumRConstraint(r, rmin=4)
    smooth = fitDr.SmoothnessConstraint(r)
    model = fitDr.buildModel((rmin_c, smooth))
    fit_result = model.fit(method='qr')
    gr = fit_result.params

    assert_array_almost_equal(gr, -r)


def test_polynomial_fit(nr=100):
    r = np.linspace(0.1, 10, nr)
    rmin_c = fitDr.MinimumRConstraint(r, rmin=3, weight=1e8)
    qmin_c = fitDr.MinimumQConstraint(r, dq=0.001, qmin=1.5, weight=1e2)
    smooth = fitDr.SmoothnessConstraint(r, weight=1)
    nrmin = len(rmin_c.y)
    gr_ideal = np.zeros_like(r)
    gr_ideal[:nrmin] = rmin_c.y
    gr_ideal[nrmin:] = np.random.rand(nr - nrmin) * 10
    q1 = np.linspace(0.02, 4, 200)
    q2 = np.linspace(3, 300, 1000)
    sq1 = bogusSQdata_Gr(q1, r, gr_ideal)
    sq2 = bogusSQdata_Gr(q2, r, gr_ideal)

    sq1.sexpt += 2.2 * q1**-1
    sq2.sexpt += 0.3 - 1.7 * q2**1

    sqd1 = fitDr.SQDataConstraint(r, sq1, instparams=None)
    sqd2 = fitDr.SQDataConstraint(r, sq2, instparams=None)
    model, npoly = fitDr.buildModel((rmin_c, sqd1, sqd2, smooth, qmin_c),
                                    fit_polynomial=[-1, 0, 1])
    assert_equal(npoly, 6)
    fit_result = model.fit(method='qr')
    gr = fit_result.params[:nr]
    poly_params = fit_result.params[nr:]

    assert_equal(len(poly_params), 6)
    assert_array_almost_equal(gr[:nrmin], gr_ideal[:nrmin], decimal=3)
    raise SkipTest  # This test isn't working right now...
    assert_array_almost_equal(gr[nrmin:], gr_ideal[nrmin:], decimal=-2)
    assert_array_almost_equal(poly_params, [2.2, 0, 0, 0, 0.3, -1.7])
