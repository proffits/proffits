import numpy as np
from numpy.testing import assert_array_almost_equal
from nose.tools import assert_equal
from nose import SkipTest

from proffits import lowmem_regression as lmreg
try:
    import statsmodels.regression.linear_model as smreg
    from statsmodels.version import version as sm_version
except ImportError:
    sm_version = None


def test_fit_straight_line(nobs=100):
    x = np.arange(nobs)
    y = 3 * x - 7
    m = np.empty((nobs, 2))
    m[:, 0] = 1
    m[:, 1] = x
    w = np.ones_like(x)
    model = lmreg.WLS(y, m, w)
    fit_results = model.fit()
    assert_array_almost_equal(fit_results.params, [-7, 3])


def test_lowmem_vs_statsmodels(nobs=100, nparams=50):
    if sm_version is None:
        raise SkipTest
    y = np.random.randn(nobs)
    m = np.random.randn(nobs, nparams) + np.tri(nobs, nparams)
    w = np.random.rand(nobs)

    lm_model = lmreg.WLS(y, m, w)
    sm_model = smreg.WLS(y, m, w)
    assert_equal(lm_model.nobs, sm_model.nobs)
    if sm_version >= '0.5':
        assert_equal(lm_model.rank, sm_model.rank)
        assert_equal(lm_model.df_model, sm_model.df_model)
    else:  # 'rank' not present in older versions
        assert_equal(lm_model.df_model, sm_model.df_model + 1)
    assert_equal(lm_model.df_resid, sm_model.df_resid)
    assert_array_almost_equal(lm_model.wexog, sm_model.wexog)
    assert_array_almost_equal(lm_model.wendog, sm_model.wendog)

    lm_fit = lm_model.fit(method='qr')
    sm_fit = sm_model.fit(method='qr')
    assert_array_almost_equal(lm_fit.params, sm_fit.params)
    assert_array_almost_equal(lm_fit.bse, sm_fit.bse)
