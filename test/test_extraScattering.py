import numpy as np
from nose.tools import assert_equal
from numpy.testing import (assert_allclose, assert_array_almost_equal,
                           assert_array_equal)

from proffits import extraScattering

import os.path
test_dir = os.path.dirname(__file__)
test_file = lambda f: os.path.join(test_dir, f)


q1 = np.linspace(0.1, 10, 500)
q2 = np.linspace(4, 50, 500)


def checkDeriv(funcobj, q, params, eps=1e-5, rtol=1e-4, atol=1e-5):
    """Check the .deriv() method against a numerical derivative"""

    # Not really to do with the derivative but here is a useful place to check
    assert_equal(funcobj.nparams, len(params))

    # If we don't do this then sometimes we get x + eps gives x for integer x
    params = np.asarray(params, dtype=float)

    if np.isscalar(eps):
        eps = np.repeat(eps, funcobj.nparams)
    if np.isscalar(rtol):
        rtol = np.repeat(rtol, funcobj.nparams)
    if np.isscalar(atol):
        atol = np.repeat(atol, funcobj.nparams)

    if q is None:
        result_0 = funcobj(params)
        jac = funcobj.deriv(params)
    else:
        result_0 = funcobj(q, params)
        jac = funcobj.deriv(q, params)

    for i, x in enumerate(params):
        params_eps = np.copy(params)
        params_eps[i] = x + eps[i]
        if q is None:
            result_eps = funcobj(params_eps)
        else:
            result_eps = funcobj(q, params_eps)
        assert_allclose((result_eps - result_0) / eps[i],
                        jac[i],
                        rtol=rtol[i], atol=atol[i])


def test_PowerLaw_SANS(q=q1, A=2.5, power=-1.2):
    sans = extraScattering.PowerLaw()
    params = (A, power)
    result = sans(q, params)
    assert_array_almost_equal(result, A * q**power)
    checkDeriv(sans, q, params)


def test_Linear0_incoherent(q=q2, k=3):
    lin = extraScattering.Linear0()
    params = (k,)
    result = lin(q, params)
    assert_array_almost_equal(result, k * q)
    checkDeriv(lin, q, params)


def test_ZeroFunc(q=q2):
    zerofunc = extraScattering.ZeroFunc()
    params = ()
    result = zerofunc(q, params)
    assert_array_almost_equal(result, np.zeros_like(q))
    checkDeriv(zerofunc, q, params)


def test_Quadratic0(q=q2, a=1.5, b=-3.2):
    quad = extraScattering.Quadratic0()
    params = (a, b)
    result = quad(q, params)
    assert_array_almost_equal(result, a * q**2 + b * q)
    checkDeriv(quad, q, params)


def test_Linear(q=q2, k=-1.2, c=2.3):
    lin = extraScattering.Linear()
    params = (k, c)
    result = lin(q, params)
    assert_array_almost_equal(result, k * q + c)
    checkDeriv(lin, q, params)


def test_Quadratic(q=q2, a=1.8, b=0.7, c=-4):
    quad = extraScattering.Quadratic()
    params = (a, b, c)
    result = quad(q, params)
    assert_array_almost_equal(result, a * q**2 + b * q + c)
    checkDeriv(quad, q, params)


def test_Weighted_PowerLaw(q=q1, A=0.7, power=-1.9):
    class Weighted_PowerLaw(extraScattering.WeightedFunc,
                            extraScattering.PowerLaw):
        pass
    weights = np.random.rand(len(q))
    wsans = Weighted_PowerLaw(weights=weights)
    params = (A, power)
    result = wsans(q, params)
    assert_array_almost_equal(result, weights * A * q**power)
    checkDeriv(wsans, q, params)


def test_SavedQ_Linear0(q=q2, k=0.34):
    class SavedQ_Linear0(extraScattering.StoredQ, extraScattering.Linear0):
        pass
    qlin = SavedQ_Linear0(q=q)
    params = (k,)
    result = qlin(params)
    assert_array_almost_equal(result, k * q)
    checkDeriv(qlin, q=None, params=params)


def test_SubtractS_PowerLaw(q=q1, A=4.4, power=-3.2):
    class SubtractS_PowerLaw(extraScattering.SubtractS,
                             extraScattering.PowerLaw):
        pass
    s = np.random.rand(len(q))
    ssans = SubtractS_PowerLaw(s=s)
    params = (A, power)
    result = ssans(q, params)
    assert_array_almost_equal(result, A * q**power - s)
    checkDeriv(ssans, q, params)


def test_MultipleFuncs(q=q2, A=1.1, power=-2, k1=3.3, k2=2.2):
    sans = extraScattering.PowerLaw()
    lin1 = extraScattering.Linear0()
    lin2 = extraScattering.Linear0()
    multi = extraScattering.MultipleFuncs((lin1, lin2), together_func=sans)
    tog_slice, sep_slices = multi.param_slices()
    assert_equal(tog_slice, slice(0, 2))
    assert_equal(sep_slices[0], slice(2, 3))
    assert_equal(sep_slices[1], slice(3, 4))
    params = (A, power, k1, k2)
    tog_params, sep_params = multi.split_params(params)
    assert_array_equal(tog_params, (A, power))
    assert_array_equal(sep_params[0], (k1,))
    assert_array_equal(sep_params[1], (k2,))

    checkresult = np.empty((2, len(q)))
    checkresult[0, :] = sans(q, (A, power)) + lin1(q, (k1,))
    checkresult[1, :] = sans(q, (A, power)) + lin2(q, (k2,))
    result = multi(q, params)
    assert_array_almost_equal(result, checkresult)
    checkDeriv(multi, q, params)


def test_MultipleFuncs_ravel(q=q2, A=1.1, power=-2, k1=3.3, k2=2.2):
    sans = extraScattering.PowerLaw()
    lin1 = extraScattering.Linear0()
    lin2 = extraScattering.Linear0()
    multi = extraScattering.MultipleFuncs((lin1, lin2), together_func=sans,
                                          ravel=True)
    params = (A, power, k1, k2)
    checkresult = np.empty((2, len(q)))
    checkresult[0, :] = sans(q, (A, power)) + lin1(q, (k1,))
    checkresult[1, :] = sans(q, (A, power)) + lin2(q, (k2,))
    result = multi(q, params)
    assert_array_almost_equal(result, np.ravel(checkresult))
    checkDeriv(multi, q, params)


def test_fit_incoherent(q=q2, const_min=20, constant=4, k1=3, k2=0.7, k3=-4.4):
    nq = len(q)
    iconst_min = np.min(np.where(q >= const_min))
    if constant is None:
        local_constant = np.random.rand()
    else:
        local_constant = constant
    s = np.ones((nq, 3)) * local_constant
    e = np.ones_like(s)
    s[:iconst_min, :] = np.random.rand(iconst_min)[:, np.newaxis]
    # Add some Infs and NaNs
    s[-100:, 0] = np.NaN
    e[-100:, 0] = np.Inf
    s[:100, 2] = np.NaN
    e[:100, 2] = np.Inf
    orig_s = np.copy(s)

    s[:, 0] += k1 * q
    s[:, 1] += k2 * q
    s[:, 2] += k3 * q

    params, corrected_s = extraScattering.fitIncoherentScattering(q, s, e,
                                                                  const_min,
                                                                  constant)
    assert_array_almost_equal(params, (k1, k2, k3))
    assert_array_almost_equal(orig_s, corrected_s)


def test_fit_incoherent_fit_constant():
    test_fit_incoherent(constant=None)


def test_fit_incoherent_and_SANS(q=np.sort(np.concatenate((q1, q2))),
                                 szero_max=2, const_min=20, constant=4,
                                 A=5, power=-1.7, k1=3, k2=0.7, k3=-4.4):
    nq = len(q)
    izero_max = np.min(np.where(q > szero_max))
    iconst_min = np.min(np.where(q >= const_min))
    if constant is None:
        local_constant = np.random.rand()
    else:
        local_constant = constant
    s = np.ones((nq, 3)) * local_constant
    e = np.ones_like(s)
    s[:iconst_min, :] = np.random.rand(iconst_min)[:, np.newaxis]
    s[:izero_max, :] = 0
    # Add some Infs and NaNs
    s[-100:, 0] = np.NaN
    e[-100:, 0] = np.Inf
    s[:100, 2] = np.NaN
    e[:100, 2] = np.Inf
    orig_s = np.copy(s)

    s[:, 0] += k1 * q
    s[:, 1] += k2 * q
    s[:, 2] += k3 * q
    s += A * q[:, np.newaxis]**power

    params, corrected_s = extraScattering.fitIncoherent_and_SANS(q, s, e,
                                                                 szero_max,
                                                                 const_min,
                                                                 constant)
    assert_array_almost_equal(params, (A, power, k1, k2, k3))
    assert_array_almost_equal(orig_s, corrected_s)
