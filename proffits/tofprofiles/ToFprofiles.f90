!> @file ToFprofiles.F90
!! @author Chris Kerr
!!
!! @brief Peak shapes for time-of-flight neutron instruments
!!
!! A full description of the functions and the rationale behind them can be
!! found in pages 143-155 of the GSAS manual, which can be found at:
!! https://subversion.xor.aps.anl.gov/EXPGUI/gsas/all/GSAS%20Manual.pdf
!!
!! This file contains an independent reimplementation of the functions in
!! "TOF profile function 2", described in pages 144-147. Implementations of
!! the other functions will be added in due course.
!!
!! @copyright Chris Kerr 2014
!! This file is part of ProfFitS.
!!
!! ProfFitS is free software: you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! ProfFitS is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with ProfFitS.  If not, see <http://www.gnu.org/licenses/>.

module tof_profile_2

!> "Perturbation coefficient" described on page 145 of the GSAS manual 
real(8), parameter :: k = 0.05d0

contains

!> G' function for the 2nd GSAS ToF profile
!!
!! Defined on page 146 of the GSAS manual
!!
!! For some reason F2py doesn't like it if I make Gprime a function
!! instead of a subroutine
pure subroutine Gprime(R, alpha, beta, sigmasq, npts, DeltaT, gfunc)
  use special_functions, only : hfunc
  implicit none
  
  integer, intent(in) :: npts
  real(8), intent(in) :: R, alpha, beta, sigmasq
  real(8), dimension(npts), intent(in) :: DeltaT
  real(8), dimension(npts), intent(out) :: gfunc
  
  real(8) :: g, rr, s, u, v !< Can't use plain r because Fortran is not case sensitive
  real(8) :: x, y, z
  
  ! Values to store the prefactors for the various brackets
  real(8) :: a, b1, b2, b3, b4
  real(8) :: inv_sigma_rt2, inv_2_sigmasq
  integer :: i
  
  x = alpha * (1 - k) - beta
  y = alpha - beta
  z = alpha * (1 + k) - beta
  
  a = (alpha * (1 - k**2) / (4 * k**2))
  b1 = a * ((2 * R * alpha**2 * beta * k**2) / (x * y * z))
  b2 = a * (1 - R * alpha * (1 + k) / z)
  b3 = a * (1 - R * alpha * (1 - k) / x)
  b4 = -2 * a * (1 - R * alpha / y)
  
  inv_2_sigmasq = 1 / (2 * sigmasq)
  inv_sigma_rt2 = sqrt(inv_2_sigmasq)
  
  do i=1,npts
    g = -(DeltaT(i)**2) * inv_2_sigmasq
    rr = (beta * sigmasq - DeltaT(i)) * inv_sigma_rt2
    s = (alpha * sigmasq - DeltaT(i)) * inv_sigma_rt2
    u = (alpha * (1 - k) * sigmasq - DeltaT(i)) * inv_sigma_rt2
    v = (alpha * (1 + k) * sigmasq - DeltaT(i)) * inv_sigma_rt2

    gfunc(i) = b1 * hfunc(g,rr) + b2 * hfunc(g,v) + b3 * hfunc(g,u) + b4 * hfunc(g,s)

  end do
end subroutine Gprime

!> L' function for the 2nd GSAS ToF profile
!!
!! Defined on page 146 of the GSAS manual
pure subroutine Lprime(R, alpha, beta, fwhm, npts, DeltaT, lfunc)
  use special_functions, only : pi, fiq
  implicit none
  
  integer, intent(in) :: npts
  real(8), intent(in) :: R, alpha, beta, fwhm
  real(8), dimension(npts), intent(in) :: DeltaT
  real(8), dimension(npts), intent(out) :: lfunc
  
  real(8) :: pre, pim, qre, qim !< Real and imaginary parts of p and q
  real(8) :: x, y, z
  
  ! Values to store the prefactors for the various brackets
  real(8) :: a, b1, b2, b3, b4
  integer :: i
  
  x = alpha * (1 - k) - beta
  y = alpha - beta
  z = alpha * (1 + k) - beta
  
  a = -(alpha * (1 - k**2) / (2 * pi * k**2 * x * y * z))
  b1 = a * (2 * R * alpha**2 * beta * k**2)
  b2 = a * (y * z * (x - R * alpha * (1 - k)))
  b3 = a * (x * y * (z - R * alpha * (1 + k)))
  b4 = -2 * a * (x * z * (y - R * alpha))
  
  pim = alpha * fwhm / 2
  qim = beta * fwhm / 2
  
  do i=1,npts
    pre = -alpha * DeltaT(i)
    qre = -beta * DeltaT(i)
    
    lfunc(i) = (b1 * fiq(qre, qim) + b2 * fiq(pre*(1-k), pim*(1-k)) &
               + b3 * fiq(pre*(1+k), pim*(1+k)) + b4 * fiq(pre, pim))
  end do
end subroutine Lprime

!> Calculate the array which, when matrix multiplied with an ideal S(Q), gives
!! the S(Q) broadened by the instrumental resolution function
!!
!! N.B. it is not declared as 'pure' because code with OpenMP annotations
!! cannot be 'pure'
subroutine peakconv_array(R, alpha, beta, fwhm, eta, T_in, npts_in, T_out, npts_out, cfunc)
  implicit none
  integer, intent(in) :: npts_in, npts_out
  real(8), dimension(npts_in), intent(in) :: R, alpha, beta, fwhm, eta, T_in
  real(8), dimension(npts_out), intent(in) :: T_out
  real(8), dimension(npts_out, npts_in), intent(out) :: cfunc

  integer :: i
  real(8) :: sigmasq
  real(8), dimension(npts_out) :: DeltaT, part_result

!$OMP PARALLEL DO private(sigmasq, DeltaT, part_result)
  do i = 1,npts_in
    sigmasq = fwhm(i)**2 / (8 * log(2d0))
    DeltaT = T_out - T_in(i)
    call Gprime(R(i), alpha(i), beta(i), sigmasq, npts_out, DeltaT, part_result)
    cfunc(:, i) = (1 - eta(i)) * part_result
    if (eta(i) > 0) then
      call Lprime(R(i), alpha(i), beta(i), fwhm(i), npts_out, DeltaT, part_result)
      cfunc(:, i) = cfunc(:, i) + eta(i) * part_result
    end if
  end do
!$OMP END PARALLEL DO
end subroutine peakconv_array

end module tof_profile_2