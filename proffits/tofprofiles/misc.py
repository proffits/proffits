"""Miscellaneous functions useful for calculating ToF profiles

Mainly includes the pseudo-Voigt function with the coefficients given by
Thompson, Cox & Hastings, J. Appl. Cryst. (1987) 20, 79-83"""

__author__ = "Chris Kerr"
__copyright__ = "Copyright Chris Kerr 2014"
__license__ = "GPL version 3+"

# This file is part of ProfFitS.
#
# ProfFitS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ProfFitS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ProfFitS.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np


def dx(x):
    """Array to be used as the denominator when calculating derivatives d/dx"""
    d = np.empty_like(x)
    d[0] = x[1] - x[0]
    d[1:-1] = (x[2:] - x[:-2]) / 2
    d[-1] = x[-1] - x[-2]
    return d


def _ensure_dims(arr):
    if np.isscalar(arr):
        return arr
    else:
        dims = list(arr.shape)
        return np.reshape(arr, dims + [1])


def ThompsonCoxHastings_gamma(gamma_gauss, gamma_lorentz):
    """Full width half maximum of the pseudo-Voigt

    Params
    ------

    gamma_gauss : float or array
        FWHM of the Gaussian

    gamma_lorentz : float or array
        FWHM of the Lorentzian"""
    gamma_gauss = _ensure_dims(gamma_gauss)
    gamma_lorentz = _ensure_dims(gamma_lorentz)
    gamma_constants = np.asarray([1.0, 2.69269, 2.42843,
                                  4.47163, 0.07842, 1.0])
    prange = np.arange(6)
    gauss_powers = gamma_gauss**(prange[::-1])
    lorentz_powers = gamma_lorentz**prange
    return np.sum(gamma_constants * gauss_powers * lorentz_powers,
                  axis=-1)**0.2


def ThompsonCoxHastings_gamma_simple(gamma_gauss, gamma_lorentz):
    gamma_sum = (gamma_gauss**5 +
                 2.69269 * gamma_gauss**4 * gamma_lorentz +
                 2.42843 * gamma_gauss**3 * gamma_lorentz**2 +
                 4.47163 * gamma_gauss**2 * gamma_lorentz**3 +
                 0.07842 * gamma_gauss * gamma_lorentz**4 +
                 gamma_lorentz**5)
    return gamma_sum**0.2


def ThompsonCoxHastings_eta(gamma_lorentz, gamma_FWHM):
    """Full width half maximum of the pseudo-Voigt

    Params
    ------

    gamma_lorentz : float or array
        FWHM of the Lorentzian

    gamma_FWHM : float or array
        FWHM returned from ThompsonCoxHastings_gamma"""
    gamma_lorentz = _ensure_dims(gamma_lorentz)
    gamma_FWHM = _ensure_dims(gamma_FWHM)
    lorentz_constants = np.asarray([1.36603, -0.47719, 0.11116])
    lorentz_ratio = gamma_lorentz / gamma_FWHM
    powers = lorentz_ratio**np.arange(1, 4)
    return np.sum(lorentz_constants * powers, axis=-1)


def ThompsonCoxHastings_PseudoVoigt(x, gamma_gauss, gamma_lorentz):
    """pseudo-Voigt function approximating the convolution of a Gaussian of
    FWHM gamma_gauss and a Lorentzian of FWHM gamma_lorentz"""
    gamma = ThompsonCoxHastings_gamma(gamma_gauss, gamma_lorentz)
    eta = ThompsonCoxHastings_eta(gamma_lorentz, gamma)

    sigmasq = gamma**2 / (8 * np.log(2))
    gaussian = np.exp(-x**2 / (2 * sigmasq)) / np.sqrt(2 * np.pi * sigmasq)
    lorentzian = (gamma / (2 * np.pi)) / ((gamma/2)**2 + x**2)

    return eta * lorentzian + (1 - eta) * gaussian
