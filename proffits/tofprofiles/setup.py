from numpy.distutils.misc_util import Configuration


def configuration(parent_package='',top_path=None):
    config = Configuration('tofprofiles', parent_package, top_path)
    
    config.add_extension('_tofprofiles',
                         extra_f90_compile_args=["-ffree-line-length-none",
                                                 "-fopenmp"],
                         extra_link_args=["-lgomp"],
                         sources=['tofprofiles.pyf',
                                  'scipy_functions.f90',
                                  'ToFprofiles.f90'])
    return config


if __name__ == '__main__':
    from numpy.distutils.core import setup
    setup(**configuration(top_path='').todict())
