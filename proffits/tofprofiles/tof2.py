"""
Peak shapes for time-of-flight neutron diffraction instruments

A full description of the functions and the rationale behind them can be
found in pages 143-155 of the GSAS manual, which can be found at:
https://subversion.xor.aps.anl.gov/EXPGUI/gsas/all/GSAS%20Manual.pdf

This file contains an independent reimplementation of the functions in
"TOF profile function 2", described in pages 144-147. Implementations of
the other functions will be added in due course.
"""

__author__ = "Chris Kerr"
__copyright__ = "Copyright Chris Kerr 2014"
__license__ = "GPL version 3+"

# This file is part of ProfFitS.
#
# ProfFitS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ProfFitS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ProfFitS.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np
from numpy import exp
import scipy.special
import scipy.integrate

try:
    import numexpr
    have_numexpr = True
    ne_fIq = numexpr.NumExpr("""1 / (z + 1 / (1 + 1 /
                                    (z + 2 / (1 + 2 /
                                    (z + 3 / (1 + 3 /
                                    (z + 4 / (1 + 4 / z))))))))""",
                             signature=[('z', complex)])
except ImportError:
    have_numexpr = False

try:
    from ._tofprofiles import tof_profile_2
    have_ftn_tof2 = True
except ImportError:
    have_ftn_tof2 = False

from . import misc


def S_k(alpha, tau):
    """S_k function for the 2nd GSAS ToF profile

    This represents "the slowing spectrum from the moderator"
    Defined on page 145 of the GSAS manual"""
    return alpha**3 * tau**2 * exp(-alpha*tau) / 2


def R_k(R, beta, tau, dtau):
    """R_k function for the 2nd GSAS ToF profile

    The delta function represents fast neutrons and the exp(-beta * tau)
    part represents slow neutrons.
    Defined on page 145 of the GSAS manual"""
    if tau == 0:
        return R + (1 - R) / dtau  # Should be (1 - R) * delta(tau)
    else:
        return R * exp(-beta * tau)


def I_k1(R, alpha, beta, t):
    """I_k Ikeda-Carpenter peak function, used in the 2nd GSAS ToF profile

    Defined on page 145 of the GSAS manual
    This is the first form of the function given"""

    bracket1 = (1 + t * (alpha - beta) + t**2 * (alpha - beta)**2 / 2)
    bracket2 = exp(-beta * t) - bracket1 * exp(-alpha * t)
    bracket3 = ((1 - R) * t**2 * exp(-alpha * t) +
                (2 * beta * R / (alpha - beta)**3) * bracket2)
    return (alpha**3 / 2) * bracket3


def I_k2(R, alpha, beta, t, k=0.05):
    """I_k Ikeda-Carpenter peak function, used in the 2nd GSAS ToF profile

    Defined on page 145 of the GSAS manual
    This is the second form of the function given"""

    x = alpha * (1 - k) - beta
    y = alpha - beta
    z = alpha * (1 + k) - beta

    bracket1 = (exp(-alpha * t * (1 - k)) +
                exp(-alpha * t * (1 + k)) -
                2 * exp(-alpha * t))
    part1 = (alpha * (1 - k**2) / (2 * k**2)) * bracket1

    # I think there is a typo in the manual in the 1/z or 1/x parts
    # In the manual it has (1 + k) in the (1 / x) part
    bracket2 = ((2 * alpha**2 * k**2 / (x * y * z)) * exp(-beta * t) -
                (1 / z) * exp(-alpha * t * (1 + k)) -
                (1 / x) * exp(-alpha * t * (1 - k)) +
                (2 / y) * exp(-alpha * t))
    part2 = (alpha * beta * (1 - k**2) / (2 * k**2)) * bracket2

    return (1 - R) * part1 + R * part2


def Gprime(R, alpha, beta, sigmasq, DeltaT, k=0.05):
    """G' function for the 2nd GSAS ToF profile

    Defined on page 146 of the GSAS manual"""

    def h(g, r):
        """h(g, r) defined on page 146 of the GSAS manual"""
        if hasattr(scipy.special, 'erfcx'):
            rpart = scipy.special.erfcx(r)
            gpart = exp(g)
        else:
            rpart = scipy.special.erfc(r)
            gpart = exp(g + r**2)
        # workaround to avoid infinities and NaNs
        valid = np.logical_and(np.isfinite(rpart), np.isfinite(gpart))
        ans = np.zeros_like(r)
        ans[valid] = gpart[valid] * rpart[valid]
        return ans

    sigma = np.sqrt(sigmasq)

    g = -(DeltaT**2) / (2 * sigmasq)
    r = (beta * sigmasq - DeltaT) / (sigma * np.sqrt(2))
    s = (alpha * sigmasq - DeltaT) / (sigma * np.sqrt(2))
    u = (alpha * (1 - k) * sigmasq - DeltaT) / (sigma * np.sqrt(2))
    v = (alpha * (1 + k) * sigmasq - DeltaT) / (sigma * np.sqrt(2))

    x = alpha * (1 - k) - beta
    y = alpha - beta
    z = alpha * (1 + k) - beta

    bracket = (((2 * R * alpha**2 * beta * k**2) / (x * y * z)) * h(g, r) +
               (1 - R * alpha * (1 + k) / z) * h(g, v) +
               (1 - R * alpha * (1 - k) / x) * h(g, u) -
               2 * (1 - R * alpha / y) * h(g, s))
    return (alpha * (1 - k**2) / (4 * k**2)) * bracket


def Gprime_range(R, alpha, beta, sigmasq, cutoff):
    drop = -np.log(cutoff)
    gausswidth = 2 * sigmasq * np.sqrt(drop)
    alphawidth = (drop + 3 * np.log(alpha)) / alpha
    alphawidth *= (1 + 2 * np.log(alphawidth))
    betawidth = drop / beta
    return (-gausswidth, gausswidth + alphawidth + betawidth)


def Lprime(R, alpha, beta, gamma, DeltaT, k=0.05, use_numexpr=have_numexpr):
    """L' function for the 2nd GSAS ToF profile

    Defined on page 146 of the GSAS manual"""

    def I(q):
        """I(q) defined on page 147 of the GSAS manual"""
        qthreshold = 500
        islow = (abs(q) <= qthreshold)
        ishigh = (abs(q) > qthreshold)
        zans = np.empty_like(q)

        # Use this slow implementation for high q where we might hit
        # problems with Inf * 0 = NaN
        if np.any(ishigh):
            highq = q[ishigh]
            if use_numexpr:
                zans[ishigh] = ne_fIq(highq)
            else:
                niter = 4 + int(128 / qthreshold)
                tmp = niter + 1
                for i in range(niter, 0, -1):
                    tmp = i / (highq + tmp)
                    tmp = i / (1 + tmp)
                zans[ishigh] = 1 / (highq + tmp)

        if np.any(islow):
            lowq = q[islow]
            zans[islow] = exp(lowq) * scipy.special.exp1(lowq)

        return zans.imag

    p = -alpha * DeltaT + alpha * gamma * 0.5j
    q = -beta * DeltaT + beta * gamma * 0.5j

    x = alpha * (1 - k) - beta
    y = alpha - beta
    z = alpha * (1 + k) - beta

    bracket = (2 * R * alpha**2 * beta * k**2 * I(q) +
               y * z * (x - R * alpha * (1 - k)) * I(p * (1 - k)) +
               x * y * (z - R * alpha * (1 + k)) * I(p * (1 + k)) -
               2 * x * z * (y - R * alpha) * I(p))
    # The manual doesn't have a minus sign here, but without the minus sign
    # it gives the wrong answer
    return -(alpha * (1 - k**2) / (2 * np.pi * k**2 * x * y * z)) * bracket


def Lprime_range(R, alpha, beta, gamma, cutoff):
    drop = -np.log(cutoff)
    lorentzwidth = np.sqrt((1 / cutoff) - 1) * gamma
    alphawidth = (drop + 3 * np.log(alpha)) / alpha
    alphawidth *= (1 + 2 * np.log(alphawidth))
    betawidth = drop / beta
    return (-lorentzwidth, lorentzwidth + alphawidth + betawidth)


def ToF_profile_2(params, d_peak, T, normalise=True,
                  use_ftn_tof2=have_ftn_tof2, use_numexpr=have_numexpr):
    """The peak function for the 2nd GSAS ToF profile

    Defined on page 144-147 of the GSAS manual

    Params
    ------

    params : instance of an instrument parameters class

    d_peak : float
        the d-spacing of the peak under consideration

    T : float or array
        the time-of-flight value(s) at which to calculate the peak function

    normalise : bool
        If False, the integrated area under the peak function is 1.
        If True, the integrated area under the peak function is:
            d_peak**4 * sin(theta)"""

    if isinstance(T, np.ndarray):
        Tarray = True
    else:
        T = np.asarray([T])
        Tarray = False

    T_peak = params.T_from_d(d_peak)
    DeltaT = T - T_peak

    alpha = params.alpha(d_peak)
    beta = params.beta(d_peak)
    R = params.R(d_peak)
    sigmasq_gauss = params.sigmasq_gauss(d_peak)
    gamma_lorentz = params.gamma_lorentz(d_peak)

    assert params.lambd(d_peak) > 0
    assert T_peak > 0
    assert alpha > 0
    assert beta > 0
    assert R > 0
    assert sigmasq_gauss > 0
    assert gamma_lorentz >= 0  # Some banks have all gamma params == 0

    gamma_gauss = np.sqrt(8 * np.log(2) * sigmasq_gauss)

    fwhm = misc.ThompsonCoxHastings_gamma(gamma_gauss, gamma_lorentz)
    eta = misc.ThompsonCoxHastings_eta(gamma_lorentz, fwhm)
    sigmasq = fwhm**2 / (8 * np.log(2))
    assert fwhm > 0
    assert eta <= 1
    assert eta >= 0

    if use_ftn_tof2:
        Lpart = tof_profile_2.lprime(R, alpha, beta, fwhm, DeltaT)
        Gpart = tof_profile_2.gprime(R, alpha, beta, sigmasq, DeltaT)
    else:
        Lpart = Lprime(R, alpha, beta, fwhm, DeltaT, use_numexpr=use_numexpr)
        Gpart = Gprime(R, alpha, beta, sigmasq, DeltaT)

    # Diagnostic assertions
    assert np.all(Lpart >= 0)
    assert np.all(Gpart >= -1e-200)  # Can get very small negative numbers
    Gpart = np.maximum(Gpart, 0)

    peak = eta * Lpart + (1 - eta) * Gpart
    if normalise:
        # The desired area is called florentz in RMcprofile
        desired_area = d_peak**4 * np.sin(params.theta)
        peak *= desired_area

    if Tarray:
        return peak
    else:
        return peak[0]


def peakconv_array_ftn(params, d_in, T_out):
    T_in = params.T_from_d(d_in)
    assert np.all(T_in > 0)
    assert np.all(T_out > 0)

    alpha = params.alpha(d_in)
    beta = params.beta(d_in)
    R = params.R(d_in)
    sigmasq_gauss = params.sigmasq_gauss(d_in)
    gamma_lorentz = params.gamma_lorentz(d_in)

    assert np.all(params.lambd(d_in) > 0)
    assert np.all(alpha > 0)
    assert np.all(beta > 0)
    assert np.all(R > 0)
    assert np.all(sigmasq_gauss > 0)
    assert np.all(gamma_lorentz >= 0)

    gamma_gauss = np.sqrt(8 * np.log(2) * sigmasq_gauss)

    fwhm = misc.ThompsonCoxHastings_gamma(gamma_gauss, gamma_lorentz)
    eta = misc.ThompsonCoxHastings_eta(gamma_lorentz, fwhm)

    assert np.all(fwhm > 0)
    assert np.all(eta <= 1)
    assert np.all(eta >= 0)

    cfunc = tof_profile_2.peakconv_array(R, alpha, beta, fwhm, eta,
                                         T_in, T_out)
    # Sometimes rounding errors give results slightly less than 0
    assert np.all(cfunc >= -1e-100)
    cfunc = np.maximum(cfunc, 0)
    return cfunc


def peakconv_array_ToF2(params, Qin, Qout=None,
                        use_ftn_tof2=have_ftn_tof2, use_numexpr=have_numexpr):
    """The array which, when matrix multiplied with a S_ideal(Qin) vector,
    gives S(Qout) with that scattering convolved with the 2nd ToF profile

    Params
    ------

    params : instance of an instrument parameters class

    Qin : array
        Q points where the S_ideal(Q) is sampled

    Qout : array
        Q points where the S_convolved(Q) should be calculated""" 
    if Qout is None:
        Qout = Qin
    nin = len(Qin)
    nout = len(Qout)

    d = 2 * np.pi / Qin
    dout = 2 * np.pi / Qout
    T = params.T_from_d(dout)

    dT = -misc.dx(T)  # T is in reverse order
    dQout = misc.dx(Qout)
    dQin = misc.dx(Qin)

    assert np.all(dT > 0)
    assert np.all(dQout > 0)
    assert np.all(dQin > 0)

    if use_ftn_tof2:
        cfunc = peakconv_array_ftn(params, d, T)
    else:
        cfunc = np.zeros((nout, nin))
        for i in range(nin):
            cfunc[:, i] = ToF_profile_2(params, d[i], T, normalise=False,
                                        use_ftn_tof2=use_ftn_tof2,
                                        use_numexpr=use_numexpr)
    for i in range(nin):
        # \int cfunc dT is 1, we need \int cfunc dQ to be 1
        cfunc[:, i] *= (dT / dQout) * dQin[i]

    return cfunc