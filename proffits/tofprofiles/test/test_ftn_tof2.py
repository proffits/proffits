import numpy as np
from numpy.testing import assert_array_almost_equal

import scipy.special
from nose import SkipTest

from .test_tof2 import FakeInstParams
from .. import tof2
from .._tofprofiles import tof_profile_2 as ftn_tof2


def test_Gprime(ntests=10, npts=1000):
    if not hasattr(scipy.special, 'erfcx'):
        # The python Gprime is useless, skip the test
        raise SkipTest
    R = np.random.rand(ntests)
    alpha = np.abs(np.random.randn(ntests)) * 20 + 10
    beta = np.abs(np.random.randn(ntests)) * 5 + 5
    sigsq = np.abs(np.random.randn(ntests)) * 20 + 10

    llimit, ulimit = tof2.Gprime_range(R, alpha, beta, sigsq, cutoff=1e-3)

    for i in range(ntests):
        DeltaT = np.linspace(llimit[i], ulimit[i], npts)

        py_Gprime = tof2.Gprime(R[i], alpha[i], beta[i], sigsq[i], DeltaT)
        ftn_Gprime = ftn_tof2.gprime(R[i], alpha[i], beta[i], sigsq[i], DeltaT)

        assert_array_almost_equal(py_Gprime, ftn_Gprime)


def test_Lprime(ntests=10, npts=1000):
    R = np.random.rand(ntests)
    alpha = np.abs(np.random.randn(ntests)) * 20 + 10
    beta = np.abs(np.random.randn(ntests)) * 5 + 5
    fwhm = np.abs(np.random.randn(ntests)) * 20 + 10

    llimit, ulimit = tof2.Lprime_range(R, alpha, beta, fwhm, cutoff=1e-3)

    for i in range(ntests):
        DeltaT = np.linspace(llimit[i], ulimit[i], npts)

        py_Lprime = tof2.Lprime(R[i], alpha[i], beta[i], fwhm[i], DeltaT)
        ftn_Lprime = ftn_tof2.lprime(R[i], alpha[i], beta[i], fwhm[i], DeltaT)

        assert_array_almost_equal(py_Lprime, ftn_Lprime)


def test_fortran_peakconv_peakshape(npts=800, nout=600):
    Q = np.linspace(2, 10, npts)
    Qout = np.linspace(3, 8, nout)

    npy_cfunc = tof2.peakconv_array_ToF2(FakeInstParams(), Q[100:200], Qout,
                                         use_ftn_tof2=False)
    ftn_cfunc = tof2.peakconv_array_ToF2(FakeInstParams(), Q, Qout,
                                         use_ftn_tof2=True)

    assert_array_almost_equal(npy_cfunc, ftn_cfunc[:, 100:200])
