# -*- coding: utf-8 -*-
"""
Created on Tue Apr 15 12:45:19 2014

@author: chris
"""

import numpy as np
from numpy.testing import assert_array_almost_equal
from nose.tools import assert_equal, assert_almost_equal

from .. import misc


def test_dx():
    x = np.linspace(0, 10, num=101)
    dx = misc.dx(x)
    dx2 = misc.dx(x**2)
    assert_array_almost_equal(dx2[1:-1], 2 * x[1:-1] * dx[1:-1])
    assert_almost_equal(dx2[0], dx[0]**2)
    assert_almost_equal(dx2[-1], 2 * x[-1] * dx[-1] - dx[0]**2)


def test_gammas_equal(ntests=1000):
    gg = np.abs(np.random.randn(ntests))
    gl = np.abs(np.random.randn(ntests))
    simple_res = misc.ThompsonCoxHastings_gamma_simple(gg, gl)
    gamma_arr_res = misc.ThompsonCoxHastings_gamma(gg, gl)
    assert_array_almost_equal(simple_res, gamma_arr_res)
    for i in range(ntests):
        assert_almost_equal(misc.ThompsonCoxHastings_gamma(gg[i], gl[i]),
                            simple_res[i])


def test_eta_arr(ntests=1000):
    gl = np.abs(np.random.randn(ntests))
    fwhm = np.abs(np.random.randn(ntests))
    arr_eta = misc.ThompsonCoxHastings_eta(gl, fwhm)
    for i in range(ntests):
        assert_almost_equal(misc.ThompsonCoxHastings_eta(gl[i], fwhm[i]),
                            arr_eta[i])


def test_PseudoVoigt():
    """Check some properties of the pseudo Voigt function

    Doesn't actually test that the correct value is returned"""
    x = np.linspace(-10, 10, 1001, endpoint=True)
    gamma_gauss = 0.7
    gamma_lorentz = 1.3
    pv = misc.ThompsonCoxHastings_PseudoVoigt(x, gamma_gauss, gamma_lorentz)
    assert_equal(np.argmax(pv), 500)  # Check peak is at 0
    assert_array_almost_equal(pv[500:], pv[500::-1])  # Check it is symmetric
