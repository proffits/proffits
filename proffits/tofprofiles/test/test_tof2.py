import warnings
warnings.filterwarnings(action='error', category=RuntimeWarning)

import numpy as np
from scipy.integrate import simps
from nose.tools import assert_equal, assert_greater, assert_almost_equal

import scipy.special
from nose import SkipTest

from .. import tof2
from ..tof2 import ToF_profile_2


class FakeInstParams:
    @property
    def theta(self):
        return 1

    def lambd(self, d):
        return 2 * d * np.sin(1)

    def T_from_d(self, d):
        return 1000 * d

    def sigmasq_gauss(self, d):
        return 6 + 0.01 * d**2

    def gamma_lorentz(self, d):
        return np.maximum(0, 0.1 * d**2 - d)

    def alpha(self, d):
        return 10 / (5 + d)

    def beta(self, d):
        return np.ones_like(d) / 3

    def R(self, d):
        return np.exp(-80 / d**2)


def test_tof2_single_point():
    value = ToF_profile_2(FakeInstParams(), d_peak=3.0, T=4500)
    assert_greater(value, 0)


def test_tof2_many_T_points():
    times = np.linspace(3000, 6000, num=1000)
    values = ToF_profile_2(FakeInstParams(), d_peak=4.5, T=times,
                           normalise=False)
    assert (values >= 0).all()
    assert_almost_equal(simps(values, x=times), 1, places=5)


def test_Lprime_cutoff():
    ntests = 100
    R = np.random.rand(ntests)
    alpha = np.abs(np.random.randn(ntests)) * 20 + 1
    beta = np.abs(np.random.randn(ntests)) * 5 + 1
    gamma = np.abs(np.random.randn(ntests)) * 20 + 1
    cutoff = 10**(-2 - 2 * np.random.rand(ntests))

    llimit, ulimit = tof2.Lprime_range(R, alpha, beta, gamma, cutoff)

    lvalue = tof2.Lprime(R, alpha, beta, gamma, llimit)
    # Guess that the peak will be at around 3 / alpha
    zvalue = tof2.Lprime(R, alpha, beta, gamma, 3 / alpha)
    uvalue = tof2.Lprime(R, alpha, beta, gamma, ulimit)

    assert np.all(lvalue <= cutoff * zvalue)
    assert np.all(uvalue <= cutoff * zvalue)


def test_Gprime_cutoff():
    if not hasattr(scipy.special, 'erfcx'):
        # The python Gprime is useless, skip the test
        raise SkipTest
    ntests = 100
    R = np.random.rand(ntests)
    alpha = np.abs(np.random.randn(ntests)) * 20 + 1
    beta = np.abs(np.random.randn(ntests)) * 5 + 1
    gamma = np.abs(np.random.randn(ntests)) * 20 + 1
    cutoff = 10**(-2 - 2 * np.random.rand(ntests))

    llimit, ulimit = tof2.Gprime_range(R, alpha, beta, gamma, cutoff)

    lvalue = tof2.Gprime(R, alpha, beta, gamma, llimit)
    zvalue = tof2.Gprime(R, alpha, beta, gamma, 0)
    uvalue = tof2.Gprime(R, alpha, beta, gamma, ulimit)

    assert np.all(lvalue <= cutoff * zvalue)
    assert np.all(uvalue <= cutoff * zvalue)


def test_peakconv_peakshape(npts=300, nout=400):
    Q = np.linspace(2, 10, npts)
    Qout = np.linspace(3, 8, nout)

    tof2_cfunc = tof2.peakconv_array_ToF2(FakeInstParams(), Q, Qout)
    # Just check it runs for now
    assert np.all(tof2_cfunc >= 0)
    fake_Sq_ideal = np.ones_like(Q)
    fake_Sq_real = np.dot(tof2_cfunc, fake_Sq_ideal)
    assert_equal(len(fake_Sq_real), nout)
