import numpy as np
import scipy.special
from numpy.testing import assert_allclose
from nose.tools import assert_almost_equal

from .._tofprofiles import special_functions


def test_fIq(ntests=1000):
    x = 100 * np.random.randn(ntests)
    y = np.random.uniform(0.001, 100, ntests)  # only valid for positive y
    z = x + 1j * y

    scipy_ans = (np.exp(z) * scipy.special.exp1(z)).imag
    fiq_ans = np.empty_like(scipy_ans)
    for i in range(ntests):
        fiq_ans[i] = special_functions.fiq(x[i], y[i])
        try:
            assert_almost_equal(fiq_ans[i], scipy_ans[i], places=6)
        except AssertionError:
            print("fIq mismatch at x = %f, y = %f (i = %d)" % (x[i], y[i], i))
            raise

    assert_allclose(fiq_ans, scipy_ans, rtol=1e-7, atol=1e-7)


def test_hfunc(ntests=1000):
    g = np.random.uniform(-100, 0, ntests)
    if hasattr(scipy.special, 'erfcx'):
      r = np.random.uniform(-20, +100, ntests)
      scipy_ans = np.exp(g) * scipy.special.erfcx(r)
    else:
      r = np.random.uniform(-20, +20, ntests)
      scipy_ans = np.exp(g + r**2) * scipy.special.erfc(r)

    fort_ans = np.empty_like(scipy_ans)
    for i in range(ntests):
      fort_ans[i] = special_functions.hfunc(g[i], r[i])

    assert_allclose(fort_ans, scipy_ans)