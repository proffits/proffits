"""
The weighted least squares code in statsmodels uses up lots of memory
calculating things not needed by ProfFitS

This file contains some minimally-compatible classes which keep only the
relevant features of the code from statsmodels.regression.linear_model
"""

__authors__ = "Statsmodels Developers, Chris Kerr"

__copyright__ = "Statsmodels Developers 2009-2012, Chris Kerr 2014"

__license__ = "3-clause BSD"

# Some of the code in this file has been copied from the file linear_model.py
# in the statsmodels package, other parts have been rewritten.

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   a. Redistributions of source code must retain the above copyright notice,
#      this list of conditions and the following disclaimer.
#   b. Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#   c. Neither the name of Statsmodels nor the names of its contributors
#      may be used to endorse or promote products derived from this software
#      without specific prior written permission.
#
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL STATSMODELS OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
# DAMAGE.


import numpy as np


class lowmemResults(object):
    """Stores the results of WLS.fit()

    Attributes
    ----------

    params : the parameters fitted to the model

    bse : the standard errors of the parameters"""

    def __init__(self, params, bse):
        self.params = params
        self.bse = bse


class WLS(object):
    """Implements some of the statsmodels WLS interface but uses less memory

    Only allows fitting using the 'qr' method

    Deletes all the internal variables as it goes along, so it cannot be
    reused after fit() has been called"""

    def __init__(self, endog, exog, weights=1):
        n, p = exog.shape
        if np.isscalar(weights):
            weights = weights * np.ones_like(endog)
        self.wendog = endog * np.sqrt(weights)
        self.wexog = exog * np.sqrt(weights[:, np.newaxis])

        self.nobs = self.wexog.shape[0]
        self.rank = np.linalg.matrix_rank(self.wexog)
        self.df_model = self.rank
        self.df_resid = self.nobs - self.rank

    def fit(self, method='qr', want_std_errs=True):
        if method != 'qr':
            raise NotImplementedError("Only the 'qr' method is supported")

        Q, R = np.linalg.qr(self.wexog)
        if not want_std_errs:
            del self.wexog
        # else: Can't delete wexog yet, need it to calculate wresid

        effects = np.dot(Q.T, self.wendog)
        del Q
        params = np.linalg.solve(R, effects)

        if want_std_errs:
            wresid = self.wendog - np.dot(self.wexog, params)
            del self.wexog
            err_scale = np.dot(wresid, wresid) / self.df_resid
            # No need to del wresid and wendog since they are 1D

            # TODO do we really need to calculate the whole matrix just to get
            # the diagonal?
            # Timing tests show the inverse takes about half as long as the
            # QR decomposition so it's not that important
            RT_dot_R = np.dot(R.T, R)
            del R
            normalized_cov_params = np.linalg.inv(RT_dot_R)
            del RT_dot_R
            standard_errors = np.sqrt(err_scale *
                                      np.diag(normalized_cov_params))
            del normalized_cov_params
        else:
            standard_errors = None

        return lowmemResults(params, standard_errors)
    