from numpy.distutils.misc_util import Configuration


def configuration(parent_package='',top_path=None):
    config = Configuration('proffits', parent_package, top_path)
    
    config.add_subpackage('tofprofiles')
    config.add_subpackage('fileio')
    config.make_config_py()
    return config


if __name__ == '__main__':
    from numpy.distutils.core import setup
    setup(**configuration(top_path='').todict())
