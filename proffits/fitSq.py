""" Code for fitting an ideal S(Q) to multiple banks of measured S(Q) data

SQDataConstraint fits to the S(Q) data; there are several other classes which
handle various constraints that can be placed on the fit.
"""

__author__ = "Chris Kerr"
__copyright__ = "Copyright Chris Kerr 2015"
__license__ = "GPL version 3+"

# This file is part of ProfFitS.
#
# ProfFitS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ProfFitS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ProfFitS.  If not, see <http://www.gnu.org/licenses/>.


import numpy as np

import statsmodels.regression.linear_model as smreg
from . import lowmem_regression as lmreg

from .tofprofiles.misc import dx
from .tofprofiles import tof2


class FitConstraint(object):
    """Base class for things the S(Q) is fitted to"""

    def __init__(self, q_ideal, weight=1):
        """Do common init tasks

        y (target value for fit) and m (matrix from x to y) should already
        have been set before calling"""
        # Store the q_idl so it can be checked when concatenating
        self.q_ideal = q_ideal

        if not hasattr(self, 'w'):
            self.w = weight
        elif self.w is None:
            self.w = weight

    def transform(self, sq_ideal):
        return np.dot(self.m, sq_ideal)

    def chi2(self, sq_ideal):
        return np.sum((self.transform(sq_ideal) - self.y)**2 * self.w)

    def plot(self, sq_ideal, axes, exptfmt='b-', calcfmt='g-', weighted=False):
        """Plot the calculated and experimental y values"""
        if weighted:
            axes.plot(self.x, self.y * np.sqrt(self.w), exptfmt)
            axes.plot(self.x, self.transform(sq_ideal) * np.sqrt(self.w), calcfmt)
        else:
            axes.plot(self.x, self.y, exptfmt)
            axes.plot(self.x, self.transform(sq_ideal), calcfmt)


class SQDataConstraint(FitConstraint):
    """Fitting the ideal S(Q) to a bank of experimental S(Q) data"""

    def __init__(self, q_ideal, data, instparams, **kwargs):

        q_expt = data.q

        self.q_expt = q_expt
        self.x = q_expt
        self.y = data.sexpt

        if hasattr(data, 'sexpt_errs'):
            # From the statsmodels documentation:
            #
            # The weights are presumed to be (proportional to) the inverse of
            # the variance of the observations. That is, if the variables are
            # to be transformed by 1/sqrt(W) you must supply weights = 1/W.
            #
            # We want the data to be transformed by 1/errors
            # Therefore supply errors**-2
            self.w = data.sexpt_errs**-2
        else:
            self.w = 1

        peakconv_array = tof2.peakconv_array_ToF2(instparams, q_ideal, q_expt)
        self.m = peakconv_array

        # weight=None ensures the weightings aren't overwritten
        # (or at least ensures it is obvious if they are)
        super(SQDataConstraint, self).__init__(q_ideal, weight=None, **kwargs)


class MinimumRConstraint(FitConstraint):
    """Ensure there is nothing in the G(r) below a minimum value of r

    Use the sumcoeffs parameter if the scattering data are not normalised."""

    def __init__(self, q_ideal, dr, rmin, sumcoeffs=1, **kwargs):
        r = np.arange(dr, rmin, dr)
        self.r = r
        self.x = r
        self.y = -sumcoeffs * np.ones_like(r)

        dq = dx(q_ideal)
        QSQ_to_Dr = np.sin(r[:, np.newaxis] * q_ideal) * dq
        self.m = (QSQ_to_Dr / r[:, np.newaxis]) * q_ideal

        super(MinimumRConstraint, self).__init__(q_ideal, **kwargs)


class MinimumQConstraint(FitConstraint):
    """Ensure the S(Q) has no scattering closer than a minimum Q value

    Use the sumcoeffs parameter if the scattering data are not normalised"""

    def __init__(self, q_ideal, qmin, sumcoeffs=1, **kwargs):

        # Assert that the Q array is ordered (no reason why it shouldn't be)
        assert((q_ideal[1:] > q_ideal[:-1]).all())

        nq = len(q_ideal)
        nqmin = np.count_nonzero(q_ideal < qmin)

        self.x = q_ideal[:nqmin]
        self.y = -sumcoeffs * np.ones_like(self.x)

        # The "transform" matrix is just a section of the identity matrix
        self.m = np.zeros((nqmin, nq))
        self.m[:, :nqmin] = np.eye(nqmin)

        super(MinimumQConstraint, self).__init__(q_ideal, **kwargs)


class AsymptoteConstraint(FitConstraint):
    """Ensure the S(Q) tends to 0 as Q tends to infinity"""

    def __init__(self, q_ideal, qmin=0, weight=1e-6, **kwargs):

        # Assert that the Q array is ordered (no reason why it shouldn't be)
        assert((q_ideal[1:] > q_ideal[:-1]).all())

        nq = len(q_ideal)
        nqmin = np.count_nonzero(q_ideal < qmin)

        self.x = q_ideal[nqmin:]

        self.y = np.zeros((nq - nqmin,))

        # The "transform" matrix is just a section of the identity matrix
        self.m = np.zeros((nq - nqmin, nq))
        self.m[:, nqmin:] = np.eye(nq - nqmin)

        if np.isscalar(weight):
            self.w = weight * q_ideal[nqmin:]**2  # Fit more strongly as Q increases
        else:
            self.w = weight

        # weight=None ensures the weightings aren't overwritten
        # (or at least ensures it is obvious if they are)
        super(AsymptoteConstraint, self).__init__(q_ideal, weight=None, **kwargs)


class SmoothnessConstraint(FitConstraint):
    """Give a penalty to very bumpy/spiky S(Q)

    N.B. This isn't perfectly correct for Q with very non-uniform spacings.
    However, I don't think that is going to be a popular use case
    """

    def __init__(self, q_ideal, weight=1e-4, **kwargs):

        nq = len(q_ideal)
        dq = dx(q_ideal)

        # Can't define smoothness for the first or last points
        self.x = q_ideal[1:-1]
        self.y = np.zeros((nq - 2,))

        many_ones = np.ones((nq,))

        # A matrix with all 2s on the diagonal and -1s next to the diagonal
        smoothing_matrix = (2 * np.diag(many_ones, 0)
                            - np.diag(many_ones[1:], 1)
                            - np.diag(many_ones[1:], -1))
        # Remove the top and bottom row (first and last Q points)
        self.m = smoothing_matrix[1:-1, :] / dq[1:-1, np.newaxis]**2

        super(SmoothnessConstraint, self).__init__(q_ideal, weight, **kwargs)


def buildModel(constraints, use_statsmodels=False, fit_polynomial=None):
    nconstraints = len(constraints)
    # Check all r values are the same
    q0 = constraints[0].q_ideal
    for fit in constraints[1:]:
        if (fit.q_ideal != q0).any():
            raise AssertionError("Q_ideal values not the same")

    all_y = [fit.y for fit in constraints]
    all_m = [fit.m for fit in constraints]
    # Expand single weight values to the same length as y
    all_w = [fit.w * np.ones_like(fit.y) for fit in constraints]

    joined_y = np.concatenate(all_y, axis=0)
    joined_m = np.concatenate(all_m, axis=0)
    joined_w = np.concatenate(all_w, axis=0)

    if fit_polynomial is not None:
        cumsum_len = np.zeros((nconstraints + 1,), dtype=int)
        cumsum_len[1:] = np.cumsum([len(y) for y in all_y])
        all_slices = [slice(cumsum_len[i], cumsum_len[i+1])
                      for i in range(nconstraints)]
        extended_q_poly = []
        ny, nr = joined_m.shape
        for i, fit in enumerate(constraints):
            if isinstance(fit, SQDataConstraint):
                for power in fit_polynomial:
                    extended_q = np.zeros((ny, 1))
                    extended_q[all_slices[i], 0] = fit.q_expt ** power
                    extended_q_poly.append(extended_q)
        joined_m = np.concatenate([joined_m] + extended_q_poly, axis=1)
        n_poly_params = len(extended_q_poly)

    del(all_m)

    if use_statsmodels:
        model = smreg.WLS(endog=joined_y, exog=joined_m, weights=joined_w)
    else:
        model = lmreg.WLS(endog=joined_y, exog=joined_m, weights=joined_w)
    if fit_polynomial is None:
        return model
    else:
        return model, n_poly_params


def fitAll(*args):
    model = buildModel(args)
    return model.fit()
