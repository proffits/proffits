"""
Helper code to apply the extraScattering fits to Gudrun data

This code really needs to be refactored to make it more general...
"""

__author__ = "Chris Kerr"
__copyright__ = "Copyright Chris Kerr 2014"
__license__ = "GPL version 3+"

# This file is part of ProfFitS.
#
# ProfFitS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ProfFitS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ProfFitS.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np

from .fileio import Gudrun
from . import extraScattering
from .extraScattering import fitSmallAngleScattering, fitIncoherent_and_SANS


class DCS_extra(Gudrun.DCS):
    """Extends the Gudrun.DCS class to subtract irrelevant scattering

    Can either fit just the small-angle scattering or the small-angle and
    incoherent scattering together"""

    def subtractSmallAngleScattering(self, fit_range, check_results=True):
        fit_results = fitSmallAngleScattering(self.q, self.data[:, 1::2],
                                              self.data[:, 2::2],
                                              fit_range)
        self.fit_results = fit_results
        fitted_params, cov, info, mesg, ier = fit_results

        # Subtract the fitted small angle scattering function
        A, power = fitted_params

        if check_results:
            # Small angle scattering should be positive
            assert A >= 0
            # Power should be between -1 and -4
            assert power <= -1
            assert power >= -4
        for igrp in range(self.ngroups):
            s = self.data[:, (1 + 2 * igrp)]
            e = self.data[:, (2 + 2 * igrp)]
            expt_range = np.logical_and((e > 0), np.isfinite(e))
            s[expt_range] -= A * self.q[expt_range]**power

        self._calculate_banks()
        return fitted_params

    def subtract_incoherent_and_SANS(self, szero_range, sconst_range,
                                     Inc_funcs=extraScattering.Linear0,
                                     SANS_func=extraScattering.PowerLaw(),
                                     Inc_x0=(0,), SANS_x0=(0, -1),
                                     fit_kw={}):
        fit_results = fitIncoherent_and_SANS(self.q, self.data[:, 1::2],
                                             self.data[:, 2::2],
                                             szero_range, sconst_range,
                                             constant=self.sumcoeffs,
                                             Inc_funcs=Inc_funcs,
                                             SANS_func=SANS_func,
                                             Inc_x0=Inc_x0, SANS_x0=SANS_x0,
                                             fit_kw=fit_kw)
        fitted_params, corrected_s = fit_results

        self.data[:, 1::2] = corrected_s

        self._calculate_banks()
        return fitted_params
