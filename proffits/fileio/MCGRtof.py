"""Compatibility code to read in the file formats used by MCGRtof

Often different versions of MCGRtof produce slightly different formats, so it
is possible this code will not understand all of them. Also, if a number is
too large to fit in the number of digits specified, Fortran will print out a
load of asterisks instead, which will cause this code to throw errors if it
tries to read it in as a number.
"""

__author__ = "Chris Kerr"
__copyright__ = "Copyright Chris Kerr 2014"
__license__ = "GPL version 3+"

# This file is part of ProfFitS.
#
# ProfFitS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ProfFitS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ProfFitS.  If not, see <http://www.gnu.org/licenses/>.

import re
from itertools import islice

import numpy as np


class MCGR_SQ_dat:
    """S(Q) data in a bankN.dat file

    N.B. I'm not entirely sure what the third column (called 'e' here)
    actually represents."""

    def __init__(self, filename):
        with open(filename, 'rb') as datafile:
            self.npts = int(next(datafile))
            self.description = next(datafile)
            self.q, self.sexpt, self.e = np.loadtxt(datafile, unpack=True)

        if not isinstance(self.description, str):  # Python 3
            self.description = str(self.description, encoding='utf8')

        # Check that the file has the number of lines it says it has
        assert len(self.q) == self.npts


class MCGR_Gr_g:
    """G(r) data in a .g file

    N.B. The description line appears to be two integers, I'm not sure
    what they mean."""

    def __init__(self, filename):
        with open(filename, 'rb') as datafile:
            self.npts = int(next(datafile))
            self.description = next(datafile)
            self.r, self.g = np.loadtxt(datafile, unpack=True)

        if not isinstance(self.description, str):  # Python 3
            self.description = str(self.description, encoding='utf8')

        # Check that the file has the number of lines it says it has
        assert len(self.r) == self.npts


class MCGR_SQ_csv:
    """S(Q) data in a _sqbankN.csv file

    N.B. The three columns seem to be different formats e.g. one is S(Q),
    one is QS(Q) and one is S(Q)+1"""

    def __init__(self, filename):
        with open(filename, 'rb') as datafile:
            header = next(datafile).strip()
            assert header == b'Q,S(Q)_exp,S(Q)_mcgr,fexp'
            (self.q, self.sexpt,
             self.scalc, self.fexpt) = np.loadtxt(datafile, unpack=True,
                                                  delimiter=',')


class MCGR_Gr_csv:
    """G(r) data in a _g.csv file"""

    def __init__(self, filename):
        with open(filename, 'rb') as datafile:
            header = next(datafile).strip()
            assert header == b'R,Average G,StD G'
            self.r, self.g, self.std_g = np.loadtxt(datafile, unpack=True,
                                                    delimiter=',')


class MCGR_out_SQpart:
    """S(Q) data included in a .out file

    This class is only meant to be called internally from MCGR_out"""

    def __init__(self, datafile):
        maybe_headers = next(datafile).strip()

        if maybe_headers.startswith(b'beta: '):
            ignore, beta = maybe_headers.split()
            maybe_headers = next(datafile).strip()
        if maybe_headers.startswith(b'alpha: '):
            ignore, alpha1, alpha2, alpha3 = maybe_headers.split()
            maybe_headers = next(datafile).strip()

        if maybe_headers == b'Q, S(Q) (MCGR), S(Q) (Expt)':
            have_extra_cols = False
        elif maybe_headers in (b'Q, S(Q) (MCGR), S(Q) (Expt) ,S(Q) (no res)',
                               b'Q, S(Q) (MCGR), S(Q) (Expt) ,S(Q) (no res), fexp, ferr'):
            have_extra_cols = True
        else:
            raise ValueError("Unexpected headers for S(Q) section: %s" % maybe_headers)
        assert next(datafile).strip() == b'CURVES'
        self.npts, ncols = map(int, next(datafile).strip().split())
        if ncols not in (2, 3):
            raise AssertionError("Expected ncols == 2 or 3, actually %d" % ncols)
        if have_extra_cols:
            (self.q, self.scalc, self.sexpt, self.s_nores, self.fexp, self.ferr
             ) = np.loadtxt(islice(datafile, self.npts), unpack=True)
        else:
            (self.q, self.scalc, self.sexpt
             ) = np.loadtxt(islice(datafile, self.npts), unpack=True)


class MCGR_out_Grpart:
    """G(r) data included in a .out file

    This class is only meant to be called internally from MCGR_out"""

    def __init__(self, datafile):
        assert next(datafile).strip() == b'r, Basis g(r)'
        assert next(datafile).strip() == b'PLOTS'
        self.npts, ncols = map(int, next(datafile).strip().split())
        assert ncols == 1  # Not 2 for some reason
        self.r, self.g = np.loadtxt(islice(datafile, self.npts),
                                    unpack=True)


class MCGR_out:
    """Results file ('.out') from MCGRtof"""

    def __init__(self, filename):
        self.sqdata = []
        self.grdata = []
        with open(filename, 'rb') as datafile:
            assert next(datafile).strip() == b'Results from MCGR'
            assert next(datafile).strip() == b'TITLE'
            self.title = next(datafile)

            maybe_nsq_line = next(datafile).strip()
            if maybe_nsq_line == b'':
                maybe_nsq_line = next(datafile).strip()

            sqmatch = re.match(b'(\\d+) +experimental data sets',
                               maybe_nsq_line)
            if sqmatch is None:
                raise ValueError("Could not match number of S(Q) data sets: %s" % maybe_nsq_line)
            self.nsq = int(sqmatch.group(1))
            for i in range(self.nsq):
                self.sqdata.append(MCGR_out_SQpart(datafile))
            assert next(datafile).strip() == b'ENDGROUP'

            maybe_ngr_line = next(datafile).strip()
            if maybe_ngr_line == b'':
                pass
            else:
                grmatch = re.match(b'(\\d+) +Basis functions',
                                   maybe_ngr_line)
                if grmatch is None:
                    raise ValueError("Could not match number of G(r) data sets: %s" % maybe_ngr_line)
                self.ngr = int(grmatch.group(1))
                for i in range(self.ngr):
                    self.grdata.append(MCGR_out_Grpart(datafile))
                assert next(datafile).strip() == b'ENDGROUP'

            # We should now have reached the end of the file
            # only blank lines should remain
            for line in datafile:
                assert line.strip() == b''

        if not isinstance(self.title, str):  # Python 3
            self.title = str(self.title, encoding='utf8')
