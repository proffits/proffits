import os
import os.path

from nose import SkipTest

# This file does not contain any tests, just utilities used by other tests
__test__ = False

test_dir = os.path.dirname(__file__)
testdata_dir = os.path.join(os.path.dirname(test_dir), 'testdata')


def testdata_file(filename):
    filepath = os.path.abspath(os.path.join(testdata_dir, filename))
    if not os.access(filepath, os.R_OK):
        raise SkipTest
    return filepath

# the testdata_file function is not a test!
testdata_file.__test__ = False
