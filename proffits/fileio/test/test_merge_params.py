from nose.tools import assert_almost_equal, assert_list_equal

from ..Gudrun import GudrunGroupFile, DCS
from .. import merge_params
from ..inst import Inst2File

from .get_testdata import testdata_file as datafile


def test_merge_grp_inst():
    groups = GudrunGroupFile(datafile('gudrun_grp.dat'))
    insts = Inst2File(datafile('GEM56239.inst'))
    (pairs, xtragrps,
     xtrainsts) = merge_params.merge_grp_with_inst(groups, insts,
                                                   extras=True)

    assert_list_equal([(mrg.igroup, mrg.bank_num) for mrg in pairs],
                      [(1, 1), (2, 2), (3, 3), (4, 4), (5, 5)])
    # N.B. bank 6 in the GEM.inst file seems to be an amalgamation of banks
    # 7 and 8 from the gudrun_grp.dat file
    assert_list_equal([grp.igroup for grp in xtragrps], [0, 6, 7])
    assert_list_equal([inst.bank_num for inst in xtrainsts], [6])


def test_merge_grp_vs_DCS():
    groups = GudrunGroupFile(datafile('gudrun_grp.dat'))
    insts = Inst2File(datafile('GEM56239.inst'))
    dcs = DCS(datafile('Fe75Co25_GEM57666.dcs01'))
    pairs = merge_params.merge_grp_with_inst(groups, insts)
    for mrg in pairs:
        dcsbank = dcs.banks[mrg.igroup]  # both start at 0
        lims = mrg.limits(Tmin=351)
        assert_almost_equal(lims['Tmin'], 351)
        assert_almost_equal(lims['Tmax'], 2e4)
        # These numbers really do not fit very well...
        assert_almost_equal(min(dcsbank.q), lims['Qmin'], delta=0.5)
        # This one fits even less well...
        assert_almost_equal(max(dcsbank.q), lims['Qmax'], delta=21)
