import numpy as np
from numpy.testing import assert_array_almost_equal
from nose.tools import assert_equal, assert_almost_equal

from .. import MCGRtof
from .get_testdata import testdata_file as datafile


def test_read_SQ_dat():
    bank1 = MCGRtof.MCGR_SQ_dat(datafile('alpo4bank1.dat'))
    assert_equal(bank1.npts, 864)
    assert_equal(bank1.description.strip(), "AlPO4 RT")


def test_read_g():
    ap_g = MCGRtof.MCGR_Gr_g(datafile('alpo4_mcgr.g'))
    assert_equal(ap_g.npts, 4000)
    assert_equal(ap_g.description.strip(), "2131         824")
    assert_array_almost_equal(ap_g.r, np.arange(0.02, 80.001, 0.02))


def test_read_SQ_csv():
    bank2 = MCGRtof.MCGR_SQ_csv(datafile('alpo4_mcgr_sqbank2.csv'))
    assert_almost_equal(bank2.q[-1], 39.970730)


def test_read_g_csv():
    ap_g = MCGRtof.MCGR_Gr_csv(datafile('alpo4_mcgr_g.csv'))
    assert_array_almost_equal(ap_g.r, np.arange(0.02, 80.001, 0.02))


def test_read_MCGR_out_giveawaypack():
    outfile = MCGRtof.MCGR_out(datafile('mcgr_giveawaypack.out'))
    assert_equal(outfile.title.strip(), 'AlPO4')
    assert_equal(outfile.nsq, 4)
    assert_equal(outfile.ngr, 1)


def test_read_MCGR_out_mere_test():
    outfile = MCGRtof.MCGR_out(datafile('mcgr_mere_test.out'))
    assert_equal(outfile.title.strip(), 'AlPO4')
    assert_equal(outfile.nsq, 4)
    assert_equal(outfile.ngr, 1)
    # Check that one of the extra columns has been read
    assert_equal(outfile.sqdata[0].fexp[0], -1.045559)