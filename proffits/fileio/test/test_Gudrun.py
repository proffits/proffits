import numpy as np
from nose.tools import assert_equal, assert_almost_equal, assert_greater

from .. import Gudrun

from .get_testdata import testdata_file as datafile


def test_DCS():
    dcs = Gudrun.DCS(datafile('Fe75Co25_GEM57666.dcs01'),
                     strip_end_points=False)
    assert_equal(dcs.ngroups, 8)
    assert_almost_equal(max(dcs.banks[7].q), 125.3865)
    # Check that the weighted average scattering is zero
    for bank in dcs.banks[1:]:
        assert_almost_equal(np.sum(bank.sexpt / bank.sexpt_errs), 0)
    bank3 = dcs.banks[3]
    assert_almost_equal(bank3.sexpt_errs[0], 0.04138346 / bank3.sumcoeffs)
    assert_almost_equal(bank3.qsexpt_errs[0],
                        0.04138346 * bank3.q[0] / bank3.sumcoeffs)


def test_Gudrun_grp():
    groups = Gudrun.GudrunGroupFile(datafile('gudrun_grp.dat'))
    assert_equal(groups.ngroups, 8)
    for grp in groups:
        assert_greater(grp.T_from_d(1), 0)  # basic sanity test
