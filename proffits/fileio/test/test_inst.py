import numpy as np
from nose.tools import (assert_greater, assert_almost_equal, assert_equal,
                        assert_sequence_equal)

from ..inst import Inst2File
from .get_testdata import testdata_file as datafile


def test_read_GEM_inst2():
    inst2 = Inst2File(datafile('GEM56239.inst'))

    assert_sequence_equal([bank.bank_num for bank in inst2], range(1, 7))

    assert_equal(inst2[1].difc, 746)
    assert_equal(inst2[2].sigmasq1, 279.49301)
    assert_equal(inst2[6].kappa, 54.588402)


def test_inst2_limits():
    inst2 = Inst2File(datafile('GEM56239.inst'))
    for bank in inst2:
        lims = bank.limits()
        assert_greater(lims['Tmax'], lims['Tmin'])
        assert_greater(lims['dmax'], lims['dmin'])
        assert_greater(lims['Qmax'], lims['Qmin'])
        assert_almost_equal(lims['Tmax'], bank.T_from_d(lims['dmax']))
        assert_almost_equal(lims['Tmin'], bank.T_from_d(lims['dmin']))
        assert_almost_equal(lims['Qmax'], 2 * np.pi / lims['dmin'])
        assert_almost_equal(lims['Qmin'], 2 * np.pi / lims['dmax'])
