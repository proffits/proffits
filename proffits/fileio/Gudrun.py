"""
Code to read in the output from Gudrun

Gudrun writes out S(Q) data in a .dcs01 file with the banks listed separately,
and a .mdcs01 file with the banks merged into one; we want the .dcs01 file.

Gudrun can also write out the scattering as a function of T, d, energy etc.
In principle we could convert those and handle them exactly the same, but in
practice Gudrun's conversion code gives slightly different results so it is
best to start from Gudrun's conversion into Q space.

This file also includes code to read and process the gudrun_grp.dat files
which list how the detectors are grouped into banks and the position of each
detector.
"""

__author__ = "Chris Kerr"
__copyright__ = "Copyright Chris Kerr 2014"
__license__ = "GPL version 3+"

# This file is part of ProfFitS.
#
# ProfFitS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ProfFitS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ProfFitS.  If not, see <http://www.gnu.org/licenses/>.

import os.path

import numpy as np
import scipy.constants


def read_comment_line(datafile, strip=True, decode=True):
    line = next(datafile)
    assert line[:2] == b'# '
    line = line[2:]
    if strip:
        line = line.strip()
    if decode and not isinstance(line, str):
        line = str(line, 'utf8')
    return line


class DCS_data_bank:
    """Represents the S(Q) data from one detector bank

    In the .dcs01 file the data is represented as F(Q) including the self
    scattering. The data is converted to normalised QS(Q) as that is what
    linToF expects"""

    def __init__(self, data, ibank, sumcoeffs=None, strip_end_points=True):
        """Extract the data for bank ibank from the data block"""
        q = data[:, 0]
        s = data[:, 2 * ibank + 1]
        e = data[:, 2 * ibank + 2]
        i_smin = np.min(np.where(np.logical_and(s != 0, np.isfinite(s))))
        i_smax = np.max(np.where(np.logical_and(s != 0, np.isfinite(s)))) + 1
        i_emin = np.min(np.where(np.logical_and(e > 0, np.isfinite(e))))
        i_emax = np.max(np.where(np.logical_and(e > 0, np.isfinite(e)))) + 1
        assert i_emin == i_smin
        assert i_emax == i_smax
        if strip_end_points:
            # Strip the first and last Q points, as these do not have a full
            # bin's worth of data
            i_emin += 1
            i_emax -= 1
        assert i_emax > i_emin
        self.ibank = ibank
        self.q = np.copy(q[i_emin:i_emax])
        self.sexpt = np.copy(s[i_emin:i_emax])
        self.sexpt_errs = np.copy(e[i_emin:i_emax])

        # Convert to normalised QS(Q)
        if sumcoeffs is None:
            # Use the weighted average of the experimental scattering
            sumcoeffs = (np.sum(self.sexpt / self.sexpt_errs) /
                         np.sum(1 / self.sexpt_errs))
        self.sexpt -= sumcoeffs
        self.sexpt /= sumcoeffs
        self.sexpt_errs /= sumcoeffs
        self.qsexpt = self.sexpt * self.q
        self.qsexpt_errs = self.sexpt_errs * self.q
        self.sumcoeffs = sumcoeffs


class DCS:
    """Represents a Gudrun .dcs01 file"""

    def __init__(self, filename, sumcoeffs=None,
                 add_NaNs=True, strip_end_points=True):
        """Open a .dcs01 file and read in the data

        The S(Q) data will be converted to normalised QS(Q) with the average
        scattering subtracted.

        N.B. The numbering of the banks starts from 0, this is because the
        first bank is normally ignored by GSAS and therefore does not appear
        in the .inst file - if the Gudrun numbering starts from 0 and the
        .inst numbering starts from 1 then the numbers match

        Params
        ------

        filename : string
            name of the file to read

        sumcoeffs : float, optional
            the expected average S(Q) value of the file. If omitted, it will
            be estimated by averaging the scattering on each bank

        add_NaNs : boolean, optional
            if set, change the values at all the data points where no
            scattering has been measured to NaN (rather than 0, which is how
            they are recorded in the .dcs01 file)

        strip_end_points : boolean, optional
            if set, cut off the first and last data points, which might not
            have a full bin's worth of scattering intensity."""
        with open(filename, 'rb') as datafile:
            self.orig_filename = read_comment_line(datafile)
            base, ext = os.path.splitext(self.orig_filename)
            if ext != '.dcs01':
                raise ValueError("""'%s' does not appear to be a .dcs01 file
    Its original filename was '%s'""" % (filename, self.orig_filename))
            self.description = read_comment_line(datafile)
            ngroups, npts = map(int, read_comment_line(datafile).split())
            n_extra_comment_lines = int(read_comment_line(datafile))
            self.extra_comments = [read_comment_line(datafile)
                                   for i in range(n_extra_comment_lines)]
            # TODO parse these extra comments somehow
            self.data = np.loadtxt(datafile)
        assert self.data.shape[0] == npts
        self.npts = npts
        assert self.data.shape[1] == 2 * ngroups + 1
        self.ngroups = ngroups
        self.q = self.data[:, 0]

        self.sumcoeffs = sumcoeffs
        self._strip_end_points = strip_end_points
        self._add_NaNs = add_NaNs

        if add_NaNs:
            for ibank in range(ngroups):
                e = self.data[:, 2 * ibank + 2]
                iqmin = np.min(np.where(e > 0))
                iqmax = np.max(np.where(e > 0)) + 1
                if strip_end_points:
                    iqmin += 1
                    iqmax -= 1
                assert iqmax > iqmin
                self.data[:iqmin, 2 * ibank + 1] = np.nan
                self.data[:iqmin, 2 * ibank + 2] = np.Inf
                self.data[iqmax:, 2 * ibank + 1] = np.nan
                self.data[iqmax:, 2 * ibank + 2] = np.Inf

        self._calculate_banks()

    def _calculate_banks(self):
        # If the end points have already been set to NaN, no need to
        # strip off the new end points too
        strip_bank = self._strip_end_points and not self._add_NaNs
        self.banks = list()
        for ibank in range(self.ngroups):
            self.banks.append(DCS_data_bank(self.data, ibank,
                                            sumcoeffs=self.sumcoeffs,
                                            strip_end_points=strip_bank))

    def __getitem__(self, key):
        return self.banks[key]

    def __iter__(self):
        return iter(self.banks)


class gudrun_grp_Bank:
    """Represents a detector bank as represented in a gudrun_grp.dat file

    This is needed to ensure that the correspondence between ToF and Q is the
    same in ProfFits as in Gudrun, and to check that the .inst file has"""

    def __init__(self, flightpath, theta, phi, igroup):
        self.flightpath = flightpath
        self.theta = theta
        self.phi = phi
        self.igroup = igroup

        # 2 * d * sin(theta), converting d from Angstrom to metre
        lambda_factor = 2 * 1e-10 * np.sin(self.theta)
        p_factor = scipy.constants.h / lambda_factor
        v_factor = p_factor / scipy.constants.value('neutron mass')
        T_factor = self.flightpath / v_factor
        self.T_factor = T_factor * 1e6  # Units are microseconds

    def T_from_d(self, d):
        # d_metres = d * 1e-10
        # m = scipy.constants.value('neutron mass')
        # L = self.flightpath
        # h = scipy.constants.h
        # T_seconds = 2 * d_metres * np.sin(self.theta) * L * m / h
        # return T_seconds * 1e6
        return self.T_factor * d

    def T_limits(self, Tmin, Tmax):
        return Tmax / self.T_factor, Tmin / self.T_factor


class GudrunGroupFile:
    def __init__(self, filename, incident_flightpath=17.0):
        """Read in a gudrun_grp.dat file

        The default incident flightpath, 17.0 m, is the value for GEM

        N.B. The numbering of the banks starts from 0, this is because the
        first bank is normally ignored by GSAS and therefore does not appear
        in the .inst file - if the gudrun_grp numbering starts from 0 and the
        .inst numbering starts from 1 then the numbers match"""
        self.filename = filename
        self.incident_flightpath = incident_flightpath
        flightpaths = []
        twothetas = []
        phis = []
        with open(filename, 'r') as datafile:
            line = next(datafile)
            if line.strip() != "Group flight paths:-":
                raise ValueError("Not a gudrun_grp.dat file; first line is '%s'"
                                 % line)
            for line in datafile:
                try:
                    flightpaths.extend(map(float, line.split()))
                except ValueError:  # Could not parse text as float
                    if line.strip() == "Group scattering angles:-":
                        break
                    else:
                        raise  # Reraise the exception
            for line in datafile:
                try:
                    twothetas.extend(map(float, line.split()))
                except ValueError:  # Could not parse text as float
                    if line.strip() == "Group azimuthal angles:-":
                        break
                    else:
                        raise  # Reraise the exception
            for line in datafile:
                try:
                    phis.extend(map(float, line.split()))
                except ValueError:  # Could not parse text as float
                    if line.strip() == "Group number of each spectrum:-":
                        break
                    else:
                        raise  # Reraise the exception
        # Check there are the same number of data points in each array
        assert len(flightpaths) == len(twothetas)
        assert len(flightpaths) == len(phis)
        self.ngroups = len(phis)
        self.flightpaths = np.array(flightpaths)
        self.thetas = np.deg2rad(np.array(twothetas) / 2)
        self.phis = np.deg2rad(phis)
        self.groups = []
        for i in range(self.ngroups):
            self.groups.append(gudrun_grp_Bank((flightpaths[i] +
                                                incident_flightpath),
                                               self.thetas[i],
                                               self.phis[i], igroup=i))

    def __iter__(self):
        return iter(self.groups)
