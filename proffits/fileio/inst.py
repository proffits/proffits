"""
Code for reading in instrument parameters from .inst files

The .inst format is just a block of numbers with no metadata. The format is
slightly different for each peak function; you have to know which peak
profile function the file was generated for.
"""

__author__ = "Chris Kerr"
__copyright__ = "Copyright Chris Kerr 2014"
__license__ = "GPL version 3+"

# This file is part of ProfFitS.
#
# ProfFitS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ProfFitS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ProfFitS.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np
from numpy import exp, cos, sin


def read_floats(instfile):
    """read the next line from a file and convert to floats"""
    return map(float, next(instfile).strip().split())


def quadratic_formula(a, b, c, both_roots=False):
    if a == 0:  # Not really a quadratic, only one root
        if b == 0:
            assert c == 0  # If all the parameters are 0, any value is OK
            return np.Inf, -np.Inf
        ans = -c / b
        if both_roots:  # Want to return (max, min) so set one to Infinity
            if b > 0:
                return np.Inf, ans
            else:
                return ans, -np.Inf
        else:
            return ans
    else:
        if both_roots:
            plusminus = np.array([1, -1]) * np.sign(a)
        else:
            plusminus = 1
        return (-b + plusminus * np.sqrt(b**2 - 4 * a * c)) / (2 * a)


class BankParams(object):
    """class representing the GSAS instrument parameters for a detector bank"""

    def __init__(self, instfile):
        """Read in bank parameters from a .inst file

        Parameters
        ==========

        instfile - an open file object where the bank information can be read
        """

        self.bank_num = int(next(instfile))

        self.difc, self.difa, self.zero, twotheta = read_floats(instfile)
        self.theta = np.deg2rad(twotheta / 2)
        # Remaining data is read by subclasses

    def lambd(self, d):
        # Can't use plain 'lambda' as a name because it is a keyword
        return 2 * d * sin(self.theta)

    def T_from_d(self, d):
        return self.zero + self.difc * d + self.difa * d**2

    def T_limits(self, Tmin, Tmax):
        """Calculate the minimum and maximum d and Q for given cutoff times

        The time units are microseconds. 20000 us corresponds to 50 Hz.
        The default Tmin of 350 us comes from looking at Gudrun .dcst01 files.

        The equation is T = zero + difc * d + difa * d**2
        This function solves for dmax and dmin given Tmax and Tmin"""
        dmin = quadratic_formula(self.difa, self.difc, self.zero - Tmin)
        dmax = quadratic_formula(self.difa, self.difc, self.zero - Tmax)
        return dmax, dmin

    def limits(self, Tmin=350, Tmax=2e4):
        dmax, dmin = self.T_limits(Tmin, Tmax)
        assert dmax > dmin
        return {'Tmin': self.T_from_d(dmin),
                'Tmax': self.T_from_d(dmax),
                'dmin': dmin, 'dmax': dmax,
                'Qmin': 2 * np.pi / dmax,
                'Qmax': 2 * np.pi / dmin}


class BankParams1(BankParams):
    """Parameters for the GSAS TOF peak function v1"""

    def __init__(self, instfile):
        super(BankParams1, self).__init__(instfile)

        self.alpha0, self.alpha1, self.beta0, self.beta1 = read_floats(instfile)

        self.sigmasq0, self.sigmasq1, self.sigmasq2 = read_floats(instfile)

        self.rstr, = read_floats(instfile)


class BankParams2(BankParams):
    """Parameters for the GSAS TOF peak function v2"""

    def __init__(self, instfile):
        super(BankParams2, self).__init__(instfile)

        # N.B. kappa is known as 'switch' in the RMCprofile code
        self.alpha0, self.alpha1, self.beta0, self.kappa = read_floats(instfile)

        self.sigmasq0, self.sigmasq1, self.sigmasq2 = read_floats(instfile)

        self.gamma0, self.gamma1, self.gamma2, self.gamma1e, self.gamma2e = read_floats(instfile)

    def alpha(self, d):
        return 1 / (self.alpha0 + self.alpha1 * self.lambd(d))

    def beta(self, d):
        return np.ones_like(d) / self.beta0

    def R(self, d):
        return exp(-81.799 / (self.kappa * self.lambd(d)**2))

    def sigmasq_gauss(self, d):
        return (self.sigmasq0 +
                self.sigmasq1 * d**2 +
                self.sigmasq2 * d**4)

    def gamma_lorentz(self, d):
        return (self.gamma0 +
                self.gamma1 * d +
                self.gamma2 * d**2 +
                self.gamma1e * d * cos(self.theta) +
                self.gamma2e * d**2 * cos(self.theta))

    def gauss_limits(self):
        """Return the upper and lower values of d between which sigmasq_gauss
        is >= 0"""
        d2max_g, d2min_g = quadratic_formula(self.sigmasq2, self.sigmasq1,
                                             self.sigmasq0, both_roots=True)
        if d2max_g > 0:
            dmax_g = np.sqrt(d2max_g)
        else:
            dmax_g = +np.Inf
        if d2min_g > 0:
            dmin_g = np.sqrt(d2min_g)
        else:
            dmin_g = 0
        return dmax_g, dmin_g

    def lorentz_limits(self):
        """Return the upper and lower values of d between which gamma_lorentz
        is >= 0"""
        return quadratic_formula(self.gamma2 + self.gamma2e * cos(self.theta),
                                 self.gamma1 + self.gamma1e * cos(self.theta),
                                 self.gamma0, both_roots=True)

    def limits(self, *args, **kwargs):
        """In addition to fulfilling BankParams.limits(), check that the
        values won't give negative gamma_lorentz or gamma_gauss"""
        super_limits = super(BankParams2, self).limits(*args, **kwargs)
        dmax_super = super_limits['dmax']
        dmin_super = super_limits['dmin']
        # Make sure the gamma_gaussian value won't be negative
        dmax_g, dmin_g = self.gauss_limits()
        # Make sure the gamma_lorentz value won't be negative
        dmax_l, dmin_l = self.lorentz_limits()
        dmax = min(dmax_g, dmax_l, dmax_super)
        dmin = max(dmin_g, dmin_l, dmin_super)
        assert dmax > dmin
        return {'Tmin': self.T_from_d(dmin),
                'Tmax': self.T_from_d(dmax),
                'dmin': dmin, 'dmax': dmax,
                'Qmin': 2 * np.pi / dmax,
                'Qmax': 2 * np.pi / dmin}


class BankParams3(BankParams):
    """Parameters for the GSAS TOF peak function v3"""

    def __init__(self, instfile):
        super(BankParams3, self).__init__(instfile)

        self.alpha1, self.beta0, self.beta1 = read_floats(instfile)

        self.sigmasq0, self.sigmasq1, self.sigmasq2 = read_floats(instfile)

        self.gamma0, self.gamma1, self.gamma2, self.gamma1e, self.gamma2e = read_floats(instfile)


class InstFile(object):
    """Represents the contents of a .inst file

    Each .inst file contains the peak parameters for several detector banks.
    You can access these parameters either by subscripting the InstFile object
    itself, in which case they are looked up by the number given as a label in
    the .inst file (usually 1-based), or by subscripting the banks attribute,
    which is a 0-based list of the banks in order of appearance"""

    def __init__(self, file_or_string, bank_type):
        if isinstance(file_or_string, str):
            file_or_string = open(file_or_string, 'r')

        self.nbanks = int(next(file_or_string))

        self.banks = list()
        self.bank_dict = dict()
        for i in range(self.nbanks):
            bank = bank_type(file_or_string)
            self.banks.append(bank)
            self.bank_dict[bank.bank_num] = bank

    def __getitem__(self, key):
        return self.bank_dict[key]

    def __iter__(self):
        return iter(self.banks)


class Inst2File(InstFile):
    def __init__(self, file_or_string):
        super(Inst2File, self).__init__(file_or_string, BankParams2)
