"""Code to merge instrument parameters read in from gudrun_grp and .inst files

The "Gudrun people" and the "GSAS people" have different definitions of banks
and use different calibration files. When using the GSAS peak profiles on data
from Gudrun it is important to do some sanity checks...
"""

__author__ = "Chris Kerr"
__copyright__ = "Copyright Chris Kerr 2014"
__license__ = "GPL version 3+"

# This file is part of ProfFitS.
#
# ProfFitS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ProfFitS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ProfFitS.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np

from .inst import BankParams2
from .Gudrun import gudrun_grp_Bank


class Merged_Gudrun_Inst2(gudrun_grp_Bank, BankParams2):
    def __init__(self, grp, inst):
        self._grp = grp
        self._inst = inst
        for name, value in inst.__dict__.items():
            self.__setattr__(name, value)
        for name, value in grp.__dict__.items():
            self.__setattr__(name, value)

    def sigmasq_gauss(self, d, minimum=0.1):
        sigmasq = super(Merged_Gudrun_Inst2, self).sigmasq_gauss(d)
        return np.maximum(sigmasq, minimum)

    def gamma_lorentz(self, d, minimum=0.0):
        gamma = super(Merged_Gudrun_Inst2, self).gamma_lorentz(d)
        return np.maximum(gamma, minimum)

    def gauss_limits(self, minimum=0.1):
        if minimum is not None and minimum >= 0:
            return np.Inf, 0
        else:
            return super(Merged_Gudrun_Inst2, self).gauss_limits()

    def lorentz_limits(self, minimum=0.0):
        if minimum is not None and minimum >= 0:
            return np.Inf, 0
        else:
            return super(Merged_Gudrun_Inst2, self).lorentz_limits()


def merge_grp_with_inst(gudrun_banks, inst_banks, theta_tol=np.deg2rad(1.5),
                        extras=False):
    """Merge the bank parameters read from a gudrun_grp.dat file with those
    read from a .inst file

    Params
    ------

    gudrun_banks : list or GudrunGroupFile object
        the bank parameters read from a gudrun_grp.dat file

    inst_banks : list or Inst2File object
        the bank parameters read from a .inst file

    theta_tol : float
        How great a difference in the theta values will be allowed while still
        considering two sets of bank parameters to apply to the same bank

    extras : bool
        if True, return lists of the unused gudrun_grp and .inst banks"""
    pairs = []
    unused_grps = list(gudrun_banks)
    unused_insts = list(inst_banks)
    for grp in gudrun_banks:
        for inst in inst_banks:
            if abs(grp.theta - inst.theta) < theta_tol:
                assert inst in unused_insts
                assert grp in unused_grps
                pairs.append(Merged_Gudrun_Inst2(grp, inst))
                unused_grps.remove(grp)
                unused_insts.remove(inst)
    if extras:
        return pairs, unused_grps, unused_insts
    else:
        return pairs
