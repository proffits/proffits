""" Code for fitting a D(r) [r * G(r)] to S(Q) data

SQDataConstraint fits to the S(Q) data; there are several other classes which
handle various constraints that can be placed on the fit.

Note that D(r) is occasionally referred to as 'gr' in this code: this is to
avoid confusion with 'dr' i.e. the spacing between points in the r array.
"""

__author__ = "Chris Kerr"
__copyright__ = "Copyright Chris Kerr 2014"
__license__ = "GPL version 3+"

# This file is part of ProfFitS.
#
# ProfFitS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ProfFitS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ProfFitS.  If not, see <http://www.gnu.org/licenses/>.


import numpy as np

import statsmodels.regression.linear_model as smreg
from . import lowmem_regression as lmreg

from .tofprofiles.misc import dx
from .tofprofiles import tof2


class FitConstraint(object):
    """Base class for things the D(r) is fitted to"""

    def __init__(self, r, weight=1):
        """Do common init tasks

        y (target value for fit) and m (matrix from x to y) should already
        have been set before calling"""
        self.r = r  # Store the r so it can be checked when concatenating

        if not hasattr(self, 'w'):
            self.w = weight
        elif self.w is None:
            self.w = weight

    def transform(self, gr):
        return np.dot(self.m, gr)

    def chi2(self, gr):
        return np.sum((self.transform(gr) - self.y)**2 * self.w)

    def plot(self, gr, axes, exptfmt='b-', calcfmt='g-', weighted=False):
        """Plot the calculated and experimental y values"""
        if weighted:
            axes.plot(self.x, self.y * np.sqrt(self.w), exptfmt)
            axes.plot(self.x, self.transform(gr) * np.sqrt(self.w), calcfmt)
        else:
            axes.plot(self.x, self.y, exptfmt)
            axes.plot(self.x, self.transform(gr), calcfmt)


class SQDataConstraint(FitConstraint):
    """Fitting the D(r) to a bank of S(Q) data"""

    def __init__(self, r, data, instparams, qideal=None, **kwargs):

        q = data.q

        if qideal is None:
            qideal = q
        else:
            # Only use the qideal values within +/- 1 Ang-1 of the measured Q
            # TODO make this range configurable
            qideal = qideal[qideal > min(q) - 1]
            qideal = qideal[qideal < max(q) + 1]
        if instparams is not None:
            inst_limits = instparams.limits()
            qideal = qideal[qideal > inst_limits['Qmin']]
            qideal = qideal[qideal < inst_limits['Qmax']]
        self.qideal = qideal

        self.q = q
        self.x = q
        if hasattr(data, 'qsexpt'):
            self.y = data.qsexpt
        else:
            self.y = q * data.sexpt  # Fit to Q.S(Q)

        if hasattr(data, 'qsexpt_errs'):
            # From the statsmodels documentation:
            #
            # The weights are presumed to be (proportional to) the inverse of
            # the variance of the observations. That is, if the variables are
            # to be transformed by 1/sqrt(W) you must supply weights = 1/W.
            #
            # We want the data to be transformed by 1/errors
            # Therefore supply errors**-2
            self.w = data.qsexpt_errs**-2
        elif hasattr(data, 'sexpt_errs'):
            self.w = (q * data.sexpt_errs)**-2
        else:
            self.w = 1

        dr = dx(r)
        sin_qr = dr * np.sin(qideal[:, np.newaxis] * r[np.newaxis, :])

        if instparams is None:
            self.m = sin_qr
        else:
            peakconv_array = tof2.peakconv_array_ToF2(instparams, qideal, q)

            # TODO check whether it is necessary to divide by q first, then
            # convolve, then multiply by q
            peakconv_array *= q[:, np.newaxis] / qideal[np.newaxis, :]
            self.m = np.dot(peakconv_array, sin_qr)

        # weight=None ensures the weightings aren't overwritten
        # (or at least ensures it is obvious if they are)
        super(SQDataConstraint, self).__init__(r, weight=None, **kwargs)


class MinimumQConstraint(FitConstraint):
    """Ensure there is no scattering below a minimum value of Q

    This fulfils the same function as the 'project' keyword in earlier
    versions.

    Use the sumcoeffs parameter if the scattering data are not normalised."""

    def __init__(self, r, dq, qmin, sumcoeffs=1, **kwargs):
        q = np.arange(dq, qmin, dq)
        self.q = q
        self.x = q
        self.y = -sumcoeffs * q

        dr = dx(r)
        self.m = np.sin(q[:, np.newaxis] * r) * dr

        super(MinimumQConstraint, self).__init__(r, **kwargs)


class MinimumRConstraint(FitConstraint):
    """Ensure the D(r) has no atoms closer than a minimum r value

    Use the sumcoeffs parameter if the scattering data are not normalised"""

    def __init__(self, r, rmin, sumcoeffs=1, **kwargs):

        # Assert that the r array is ordered (no reason why it shouldn't be)
        assert((r[1:] > r[:-1]).all())

        nr = len(r)
        nrmin = np.count_nonzero(r < rmin)

        self.x = r[:nrmin]
        # D(r) = -r if there are no correlations in that range
        self.y = -sumcoeffs * r[:nrmin]

        # The "transform" matrix is just a section of the identity matrix
        self.m = np.zeros((nrmin, nr))
        self.m[:, :nrmin] = np.eye(nrmin)

        super(MinimumRConstraint, self).__init__(r, **kwargs)


class AsymptoteConstraint(FitConstraint):
    """Ensure the D(r) tends to 0 as r tends to infinity"""

    def __init__(self, r, rmin=0, weight=1e-6, **kwargs):

        # Assert that the r array is ordered (no reason why it shouldn't be)
        assert((r[1:] > r[:-1]).all())

        nr = len(r)
        nrmin = np.count_nonzero(r < rmin)

        self.x = r[nrmin:]

        self.y = np.zeros((nr - nrmin,))

        # The "transform" matrix is just a section of the identity matrix
        self.m = np.zeros((nr - nrmin, nr))
        self.m[:, nrmin:] = np.eye(nr - nrmin)

        if np.isscalar(weight):
            self.w = weight * r[nrmin:]**2  # Fit more strongly as r increases
        else:
            self.w = weight

        # weight=None ensures the weightings aren't overwritten
        # (or at least ensures it is obvious if they are)
        super(AsymptoteConstraint, self).__init__(r, weight=None, **kwargs)


class SmoothnessConstraint(FitConstraint):
    """Give a penalty to very bumpy/spiky D(r)

    N.B. This isn't perfectly correct for r with very non-uniform spacings.
    However, I don't think that is going to be a popular use case
    """

    def __init__(self, r, weight=1e-4, **kwargs):

        nr = len(r)
        dr = dx(r)

        # Can't define smoothness for the first or last points
        self.x = r[1:-1]
        self.y = np.zeros((nr - 2,))

        many_ones = np.ones((nr,))

        # A matrix with all 2s on the diagonal and -1s next to the diagonal
        smoothing_matrix = (2 * np.diag(many_ones, 0)
                            - np.diag(many_ones[1:], 1)
                            - np.diag(many_ones[1:], -1))
        # Remove the top and bottom row (first and last r points)
        self.m = smoothing_matrix[1:-1, :] / dr[1:-1, np.newaxis]**2

        super(SmoothnessConstraint, self).__init__(r, weight, **kwargs)


def buildModel(constraints, use_statsmodels=False, fit_polynomial=None):
    nconstraints = len(constraints)
    # Check all r values are the same
    r0 = constraints[0].r
    for fit in constraints[1:]:
        if (fit.r != r0).any():
            raise AssertionError("r values not the same")

    all_y = [fit.y for fit in constraints]
    all_m = [fit.m for fit in constraints]
    # Expand single weight values to the same length as y
    all_w = [fit.w * np.ones_like(fit.y) for fit in constraints]

    joined_y = np.concatenate(all_y, axis=0)
    joined_m = np.concatenate(all_m, axis=0)
    joined_w = np.concatenate(all_w, axis=0)

    if fit_polynomial is not None:
        cumsum_len = np.zeros((nconstraints + 1,), dtype=int)
        cumsum_len[1:] = np.cumsum([len(y) for y in all_y])
        all_slices = [slice(cumsum_len[i], cumsum_len[i+1])
                      for i in range(nconstraints)]
        extended_q_poly = []
        ny, nr = joined_m.shape
        for i, fit in enumerate(constraints):
            if isinstance(fit, SQDataConstraint):
                for power in fit_polynomial:
                    extended_q = np.zeros((ny, 1))
                    extended_q[all_slices[i], 0] = fit.q ** power
                    extended_q_poly.append(extended_q)
        joined_m = np.concatenate([joined_m] + extended_q_poly, axis=1)
        n_poly_params = len(extended_q_poly)

    del(all_m)

    if use_statsmodels:
        model = smreg.WLS(endog=joined_y, exog=joined_m, weights=joined_w)
    else:
        model = lmreg.WLS(endog=joined_y, exog=joined_m, weights=joined_w)
    if fit_polynomial is None:
        return model
    else:
        return model, n_poly_params


def fitAll(*args):
    model = buildModel(args)
    return model.fit()
