"""Code for fitting small-angle scattering and incoherent scattering

Small-angle scattering typically follows a power law, with a power between
-1 and -4. Incoherent scattering can be fitted with a polynomial.

In the trials I have made so far, fitting the incoherent scattering using the
fit_polynomial keyword on fitDr.buildModel is more successful than using the
code here. Fitting the small-angle scattering is still good, though.
"""

__author__ = "Chris Kerr"
__copyright__ = "Copyright Chris Kerr 2014"
__license__ = "GPL version 3+"

# This file is part of ProfFitS.
#
# ProfFitS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ProfFitS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ProfFitS.  If not, see <http://www.gnu.org/licenses/>.

import warnings

import numpy as np
import scipy.optimize


class ExtraScattering(object):
    """Abstract base class for fitting and subtracting irrelevant scattering"""
    def __init__(self):
        # Needed otherwise we get errors saying
        # > TypeError: object.__init__() takes no parameters
        # when calling super().__init__(self, *args, **kw) in subclasses
        pass

    def __call__(self, q, params):
        raise NotImplementedError

    def deriv(self, q, params):
        raise NotImplementedError


class PowerLaw(ExtraScattering):
    """Power law function, used to fit small-angle scattering"""
    nparams = 2

    def __call__(self, q, params):
        A, power = params
        result = A * q**power
        if np.any(q <= 0):
            result[q <= 0] = 0
        return result

    def deriv(self, q, params):
        A, power = params
        deriv = np.empty((len(params), len(q)))
        # d/dA
        deriv[0, :] = q**power
        # d/d_power
        deriv[1, :] = A * np.log(q) * q**power
        if np.any(q <= 0):
            deriv[:, (q <= 0)] = 0
        return deriv


class ZeroFunc(ExtraScattering):
    """Null function that always returns zero"""
    nparams = 0

    def __call__(self, q, params):
        return np.zeros_like(q)

    def deriv(self, q, params):
        return np.zeros((0, len(q)))


class Linear0(ExtraScattering):
    """Straight line through 0, used to fit incoherent scattering"""
    nparams = 1

    def __call__(self, q, params):
        k, = params
        return k * q

    def deriv(self, q, params):
        k, = params
        deriv = np.empty((len(params), len(q)))
        deriv[0, :] = q
        return deriv


class Quadratic0(ExtraScattering):
    """Quadratic through 0, used to fit incoherent scattering"""
    nparams = 2

    def __call__(self, q, params):
        a, b = params
        return a * q**2 + b * q

    def deriv(self, q, params):
        a, b = params
        deriv = np.empty((len(params), len(q)))
        deriv[0, :] = q**2
        deriv[1, :] = q
        return deriv


class Linear(ExtraScattering):
    """Straight line, used to fit incoherent scattering"""
    nparams = 2

    def __call__(self, q, params):
        k, c = params
        return k * q + c

    def deriv(self, q, params):
        k, c = params
        deriv = np.empty((len(params), len(q)))
        deriv[0, :] = q
        deriv[1, :] = 1
        return deriv


class Quadratic(ExtraScattering):
    """Quadratic, used to fit incoherent scattering"""
    nparams = 3

    def __call__(self, q, params):
        a, b, c = params
        return a * q**2 + b * q + c

    def deriv(self, q, params):
        a, b, c = params
        deriv = np.empty((len(params), len(q)))
        deriv[0, :] = q**2
        deriv[1, :] = q
        deriv[2, :] = 1
        return deriv


class WeightedFunc(ExtraScattering):
    """Mixin to add weighting (e.g. 1 / std. error) to a scattering class"""
    def __init__(self, weights, *args, **kw):
        self.weights = weights
        super(WeightedFunc, self).__init__(*args, **kw)

    def __call__(self, q, params):
        result = super(WeightedFunc, self).__call__(q, params)
        return self.weights * result

    def deriv(self, q, params):
        result = super(WeightedFunc, self).deriv(q, params)
        return self.weights * result


class StoredQ(ExtraScattering):
    """Save the Q array so it does not have to be passed in as an argument"""
    def __init__(self, q, *args, **kw):
        self.q = q
        super(StoredQ, self).__init__(*args, **kw)

    def __call__(self, params):
        return super(StoredQ, self).__call__(self.q, params)

    def deriv(self, params):
        return super(StoredQ, self).deriv(self.q, params)


class SubtractS(ExtraScattering):
    """Subtract a saved array of values from the calculated scattering

    e.g. for a fit where the output has to be zero"""
    def __init__(self, s, removeNaNs=True, *args, **kw):
        self.s = s
        super(SubtractS, self).__init__(*args, **kw)
        if removeNaNs and np.any(np.isnan(self.s)):
            if isinstance(self, StoredQ):
                self.q = self.q[np.isfinite(self.s)]
            if isinstance(self, WeightedFunc):
                self.weights = self.weights[np.isfinite(self.s)]
            self.s = s[np.isfinite(self.s)]

    def __call__(self, q, params):
        return (super(SubtractS, self).__call__(q, params) - self.s)


class MultipleFuncs(ExtraScattering):
    """Combine multiple function classes in a single output"""
    def __init__(self, separate_funcs, together_func=None, ravel=False):
        """
        Params
        ------

        separate_funcs : list of ExtraScattering instances, one for each group
            Each function is applied to q, with the results concatenated

        together_func : ExtraScattering function
            The result of this function, if present is added to the results of
            every one of the separate_funcs"""
        self.separate_funcs = separate_funcs
        self.ngroups = len(separate_funcs)
        self.nparams = sum(func.nparams for func in separate_funcs)
        self.together_func = together_func
        if together_func is not None:
            self.nparams += together_func.nparams
        self.ravel = ravel

    def param_slices(self):
        if self.together_func is not None:
            together_start = 0
            together_end = self.together_func.nparams
        else:
            together_start = 0
            together_end = 0
        together_slice = slice(together_start, together_end)
        remaining_start = together_end
        separate_slices = []
        for func in self.separate_funcs:
            func_start = remaining_start
            func_end = remaining_start + func.nparams
            remaining_start = func_end
            separate_slices.append(slice(func_start, func_end))
        assert remaining_start == self.nparams
        return together_slice, separate_slices

    def split_params(self, params):
        """If there is a 'together_func', its parameters come first"""
        assert len(params) == self.nparams
        together_slice, separate_slices = self.param_slices()
        together_params = params[together_slice]
        separate_params = [params[sepslice] for sepslice in separate_slices]
        return together_params, separate_params

    def __call__(self, q, params):
        together_params, separate_params = self.split_params(params)
        result = np.empty((self.ngroups, len(q)), order='C')
        for i, func in enumerate(self.separate_funcs):
            result[i, :] = func(q, separate_params[i])
        if self.together_func is not None:
            result += self.together_func(q, together_params)
        if self.ravel:
            result = np.ravel(result)
        return result

    def deriv(self, q, params):
        tog_params, sep_params = self.split_params(params)
        tog_slice, sep_slices = self.param_slices()

        result = np.zeros((self.nparams, self.ngroups, len(q)), order='C')
        for i, func in enumerate(self.separate_funcs):
            result[sep_slices[i], i, :] = func.deriv(q, sep_params[i])
        if self.together_func is not None:
            tog_deriv = self.together_func.deriv(q, tog_params)
            result[tog_slice, :, :] = tog_deriv[:, np.newaxis, :]

        if self.ravel:
            result = np.reshape(result, (self.nparams, self.ngroups * len(q)))
        return result


def fitSmallAngleScattering(q, s, e, fit_range, funccls=PowerLaw, x0=(0, -1)):
    """Fit a function (by default a power law) to the small angle scattering

    Uses scipy.optimize.leastsq; returns the results of that function"""
    assert s.shape == e.shape
    if s.ndim == 1:
        s = s[:, np.newaxis]
        e = e[:, np.newaxis]
    npts, ngroups = s.shape
    assert q.shape == (npts,)

    assert len(x0) == funccls.nparams

    if np.isscalar(fit_range):
        fit_qmin = 0
        fit_qmax = fit_range
    else:
        fit_qmin, fit_qmax = fit_range

    # Extract the region of the data where Q is inside the fit range
    iqmin = np.min(np.where(q >= fit_qmin))
    iqmax = np.min(np.where(q > fit_qmax))
    s = s[iqmin:iqmax, :]
    e = e[iqmin:iqmax, :]
    q = np.repeat(q[iqmin:iqmax, np.newaxis], ngroups, axis=1)
    assert s.shape == e.shape
    assert s.shape == q.shape

    class SmallAngleFit(StoredQ, WeightedFunc, SubtractS, funccls):
        pass
    weightedPowerLaw = SmallAngleFit(q=np.ravel(q),
                                     s=np.ravel(s),
                                     weights=np.ravel(1/e))

    fit_results = scipy.optimize.leastsq(weightedPowerLaw,
                                         x0=x0,
                                         Dfun=weightedPowerLaw.deriv,
                                         col_deriv=True,
                                         full_output=True)

    fitted_params, cov, info, mesg, ier = fit_results
    if ier not in (1, 2, 3, 4):
        raise RuntimeError("Could not fit a power law small angle " +
                           "scattering function: '%s'" % mesg)
    elif ier == 4:
        # ier == 4 is supposed to be a success condition but has been
        # associated with bad fits in the past
        param_string = repr(fitted_params)
        info_string = repr(info)
        warnings.warn("""Possible bad fit to small angle scattering: '%s'
                         Fitted parameters: %s"
                         Info: %s""" % (mesg, param_string, info_string))

    return fit_results


class FitIncoherent_helper(object):
    def __init__(self, q, s, e, constant_range, constant, funcobj,
                 diff_weight=0.01):
        self.q = q
        self.s = s
        self.e = e
        self.constant_range = constant_range
        self.constant = constant
        self.funcobj = funcobj
        self.diff_weight = diff_weight

        if np.isscalar(constant_range):
            const_qmin = constant_range
            const_qmax = np.Inf
        else:
            const_qmin, const_qmax = constant_range
        self.const_iqmin = np.min(np.where(q >= const_qmin))
        self.const_iqmax = np.max(np.where(q <= const_qmax)) + 1

        self._cached_params = None

    def constant_chi2_deriv(self, resid, deriv):
        iqmin, iqmax = self.const_iqmin, self.const_iqmax
        nparams, ngrps, npts = deriv.shape
        npts_range = iqmax - iqmin

        e_part = self.e[:, iqmin:iqmax]
        resid_part = np.copy(resid[:, iqmin:iqmax])
        if self.constant is None:
            # use the mean as the local constant
            local_constant = (np.nansum(resid_part / e_part) /
                              np.nansum(1 / e_part))
            self.fitted_constant = -local_constant
        else:
            local_constant = -self.constant

        weight_part = np.ravel(e_part**-2)
        resid_part = np.ravel(resid_part - local_constant)
        deriv_part = np.reshape(deriv[:, :, iqmin:iqmax],
                                (nparams, ngrps * npts_range), order='C')

        v = np.logical_and(np.isfinite(resid_part), np.isfinite(weight_part))
        chi2 = np.dot(resid_part[v], resid_part[v] * weight_part[v])
        deriv_resid = np.dot(deriv_part[:, v], resid_part[v] * weight_part[v])
        return chi2, deriv_resid

    def diff_chi2_deriv(self, resid, deriv):
        nparams, ngrps, npts = deriv.shape

        chi2 = 0
        deriv_resid = np.zeros((nparams,))
        for i in range(1, ngrps):
            diff_weight = 1 / (self.e[i, :]**2 + self.e[i-1, :]**2)
            resid_diff = resid[i, :] - resid[i-1, :]
            deriv_diff = deriv[:, i, :] - deriv[:, i-1, :]
            v = np.logical_and(np.isfinite(resid_diff),
                               np.isfinite(diff_weight))
            chi2 += np.dot(resid_diff[v], diff_weight[v] * resid_diff[v])
            deriv_resid += np.dot(deriv_diff[:, v],
                                  diff_weight[v] * resid_diff[v])
        return chi2, deriv_resid

    def _check_cache(self, params):
        if np.any(params != self._cached_params):
            val = self.funcobj(self.q, params)
            self._cached_val = val
            deriv = self.funcobj.deriv(self.q, params)
            resid = val - self.s
            cons_chi2, cons_deriv = self.constant_chi2_deriv(resid, deriv)
            diff_chi2, diff_deriv = self.diff_chi2_deriv(resid, deriv)
            self._cached_chi2 = cons_chi2 + diff_chi2 * self.diff_weight
            self._cached_deriv = cons_deriv + diff_deriv * self.diff_weight

            self._cached_params = params

    def chi2(self, params):
        self._check_cache(params)
        return self._cached_chi2

    def deriv(self, params):
        self._check_cache(params)
        return self._cached_deriv

    def corrected_s(self, params):
        self._check_cache(params)
        return self.s - self._cached_val


def fitIncoherentScattering(q, s, e, constant_range, constant=None,
                            funcs_to_fit=Linear0, x0=(0,)):
    """Fit parameters for the incoherent scattering

    It is assumed that the scattering should be a constant over constant_range,
    while outside that range the scattering from different banks should be
    equal.
    Unlike the small angle scattering, different parameters are fitted to each
    bank of data.

    Params
    ------

    constant_range : float or 2-tuple of floats
        minimum and maximum Q for the range to fit to a constant. If only one
        value is present, that is the minimum

    constant : float or None
        Value that the scattering should equal over constant_range. If None,
        fit a value."""
    assert s.shape == e.shape
    if s.ndim == 1:
        s_1D = True
        s = s[np.newaxis, :]
        e = e[np.newaxis, :]
    else:
        s_1D = False
        # Transpose the arrays because I wrote the fitting code assuming they
        # have q in the last dimension (which is the most efficient way to do
        # the calculations)
        s = s.T
        e = e.T
    ngroups, npts = s.shape

    assert q.shape == (npts,)

    if not isinstance(funcs_to_fit, list):
        if len(x0) == funcs_to_fit.nparams:
            x0 = np.repeat(x0, ngroups)
        funcs_to_fit = [funcs_to_fit() for i in range(ngroups)]

    multiFunc = MultipleFuncs(separate_funcs=funcs_to_fit,
                              together_func=None, ravel=False)
    assert multiFunc.nparams == len(x0)

    fitHelper = FitIncoherent_helper(q, s, e, constant_range, constant,
                                     funcobj=multiFunc)

    fit_result = scipy.optimize.minimize(fitHelper.chi2, x0=x0,
                                         jac=fitHelper.deriv)
    if not fit_result.success:
        msg = fit_result.message
        if msg.startswith("Desired error not necessarily achieved"):
            warnings.warn(msg)
        else:
            raise RuntimeError('Fit was unsuccessful: "%s"' % msg)
    corrected_s = fitHelper.corrected_s(fit_result.x)
    if s_1D:
        return fit_result.x, np.ravel(corrected_s)
    else:
        # need to transpose the result back to (npts, ngroups) shape
        return fit_result.x, corrected_s.T


class Fit_Inc_SANS_helper(FitIncoherent_helper):
    """helper class for fitting both incoherent and small-angle scattering"""
    def __init__(self, q, s, e, szero_range, *args, **kw):
        super(Fit_Inc_SANS_helper, self).__init__(q, s, e, *args, **kw)

        if np.isscalar(szero_range):
            szero_qmin = 0
            szero_qmax = szero_range
        else:
            szero_qmin, szero_qmax = szero_range
        self.szero_iqmin = np.min(np.where(q >= szero_qmin))
        self.szero_iqmax = np.min(np.where(q > szero_qmax))

        self._cached_params = None

    def szero_chi2_deriv(self, resid, deriv):
        iqmin, iqmax = self.szero_iqmin, self.szero_iqmax
        nparams, ngrps, npts = deriv.shape
        npts_range = iqmax - iqmin

        e_part = self.e[:, iqmin:iqmax]
        resid_part = np.ravel(resid[:, iqmin:iqmax])
        weight_part = np.ravel(e_part**-2)
        deriv_part = np.reshape(deriv[:, :, iqmin:iqmax],
                                (nparams, ngrps * npts_range), order='C')

        v = np.logical_and(np.isfinite(resid_part), np.isfinite(weight_part))
        chi2 = np.dot(resid_part[v], resid_part[v] * weight_part[v])
        deriv_resid = np.dot(deriv_part[:, v], resid_part[v] * weight_part[v])
        return chi2, deriv_resid

    def _check_cache(self, params):
        if np.any(params != self._cached_params):
            val = self.funcobj(self.q, params)
            self._cached_val = val
            deriv = self.funcobj.deriv(self.q, params)
            resid = val - self.s
            cons_chi2, cons_deriv = self.constant_chi2_deriv(resid, deriv)
            diff_chi2, diff_deriv = self.diff_chi2_deriv(resid, deriv)
            szero_chi2, szero_deriv = self.szero_chi2_deriv(resid, deriv)
            self._cached_chi2 = (cons_chi2 + szero_chi2 +
                                 diff_chi2 * self.diff_weight)
            self._cached_deriv = (cons_deriv + szero_deriv +
                                  diff_deriv * self.diff_weight)

            self._cached_params = params


def fitIncoherent_and_SANS(q, s, e, szero_range, sconst_range, constant=None,
                           SANS_func=PowerLaw(), SANS_x0=(0, -1),
                           Inc_funcs=Linear0, Inc_x0=(0,),
                           fit_kw={}):
    """Fit parameters for the incoherent and small angle scattering

    It is assumed that the scattering should be a constant over sconst_range,
    while outside that range the scattering from different banks should be
    equal. Within szero_range the scattering should be zero.
    The small angle scattering is assumed to be the same for each bank, while
    the incoherent scattering is different.

    Params
    ------

    szero_range : float or 2-tuple of floats
        minimum and maximum Q for the range to fit to a constant. If only one
        value is present, that is the maximum

    sconst_range : float or 2-tuple of floats
        minimum and maximum Q for the range to fit to a constant. If only one
        value is present, that is the minimum

    constant : float or None
        Value that the scattering should equal over constant_range. If None,
        fit a value.

    fit_kw : dict
        keyword arguments to pass to the `minimize` function"""
    assert s.shape == e.shape
    if s.ndim == 1:
        s_1D = True
        s = s[np.newaxis, :]
        e = e[np.newaxis, :]
    else:
        s_1D = False
        # Transpose the arrays because I wrote the fitting code assuming they
        # have q in the last dimension (which is the most efficient way to do
        # the calculations)
        s = s.T
        e = e.T
    ngroups, npts = s.shape

    assert q.shape == (npts,)

    if not isinstance(Inc_funcs, list):
        if len(Inc_x0) == Inc_funcs.nparams:
            Inc_x0 = np.repeat(Inc_x0, ngroups)
        Inc_funcs = [Inc_funcs() for i in range(ngroups)]

    x0 = np.concatenate((SANS_x0, Inc_x0))

    multiFunc = MultipleFuncs(separate_funcs=Inc_funcs,
                              together_func=SANS_func, ravel=False)
    assert multiFunc.nparams == len(x0)

    fitHelper = Fit_Inc_SANS_helper(q, s, e, szero_range=szero_range,
                                    constant_range=sconst_range,
                                    constant=constant,
                                    funcobj=multiFunc)

    fit_result = scipy.optimize.minimize(fitHelper.chi2, x0=x0,
                                         jac=fitHelper.deriv,
                                         **fit_kw)
    if not fit_result.success:
        msg = fit_result.message
        if msg.startswith("Desired error not necessarily achieved"):
            warnings.warn(msg)
        else:
            raise RuntimeError('Fit was unsuccessful: "%s"' % msg)
    corrected_s = fitHelper.corrected_s(fit_result.x)
    if s_1D:
        return fit_result.x, np.ravel(corrected_s)
    else:
        # need to transpose the result back to (npts, ngroups) shape
        return fit_result.x, corrected_s.T
        