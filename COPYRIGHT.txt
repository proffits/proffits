ProfFitS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ProfFitS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ProfFitS.  If not, see <http://www.gnu.org/licenses/>.


The file scipy_functions.f90 contains code adapted from the scipy.special package

SciPy is released under a BSD licence. Some of the code does not originate in
SciPy but instead is imported from other packages which are licensed under
similarly permissive licences.


The code in lowmem_regression.py borrows heavily from the code in the file
linear_model.py in the statsmodels package. Statsmodels is released under a
BSD licence.
