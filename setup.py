# -*- coding: utf-8 -*-

from numpy.distutils.core import setup
from numpy.distutils.misc_util import Configuration


def configuration(parent_package='',top_path=None):
    config = Configuration(None, parent_package, top_path)
    config.add_subpackage('proffits')
    return config


setup(
    name='proffits',
    version='0.0.1a1',
    description='Deconvolving the instrumental resolution function from neutron scattering data from time-of-flight instruments',
    url='https://gitorious.org/proffits/proffits',
    author='Chris Kerr',
    license='GPLv3+',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Natural Language :: English'],
    packages=['proffits',
              'proffits.fileio',
              'proffits.tofprofiles'],
    install_requires=['numpy', 'scipy'],
    extras_require = {
    'test': ['nose', 'statsmodels'],
    },
    configuration=configuration
)
